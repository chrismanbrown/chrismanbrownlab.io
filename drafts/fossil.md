---
title: "github is not git but fossil is fossil"
subtitle: "a git users guide to fossil"
created_at: "Wed, 09 Feb 2022 09:32:30 -0700"
---

## introduction

I've used git my entire life to the exclusion of other kinds of version control[^patch]. Fossil is an alternative that I've heard spoken of favorably. Being a curious human person, I always idly meant to check it out some time.

Well, that time is now!

[^patch]: With the exceptions of teams that had *no* version control system. One memorable team would copy and paste code snippets into posts on basecamp to be shared with other developers. Another time, on a two person team, my partner dev would create diffs of his local work and email me the patches, which I would then need to apply to my local work, like it was the nineteen damn seventies. Now, I love archaic command line utilities as much as the next person. And granted, this allowed us to skip having to set up and configure a repo and hosting which, at this organization, required a fair amount of process and approval. So I guess in that regard it was fine. But it also felt needlessly manual.

## about

Fossil is a <abbr title="version control system">vcs</abbr> from the makers of sqlite, one of the most beloved pieces of software ever created. The fossil executable is a single C binary. The fossil repository is a single sqlite database. Fossil the program mostly has feature parity with git, with a few conceptual differences, making switching mostly trivial. And perhaps most interestingly, fossil includes a server that when launched with `fossil ui` gives you all of the features (and then some) of a git forge website: file, tag, and branch view and history, logs, tickets, a wiki *and* a forum *and* a chat room, and more.

## quickstart

First, `brew install fossil`.

You probably already have a git repo somewhere locally that you can import into fossil to get started good and quick:

- `cd my-awesome-git-repo`
- `git fast-export --all | fossil import --git fossil.db`

This will create a `fossil.db` database file in your repo. Move into a new repo somewhere and open it.

- `mv my-awesome-git-repo/fossil.db ..`
- `take ../my-awesome-fossil-repo`[^take]
- `fossil open ../fossil.db`

[^take]: `take` combines `mkdir` and `cd`, allowing you to `cd` into a directory that doesn't exist yet by creating it as part of the same command.

You should see all your files. Note, when I did this, I lost the history of my project. There must be a git export or a fossil import option I don't know about that preserves commit messages, etc. It would be quite a costly migration otherwise!

Okay, now for the fun part.

- `fossil ui`

Boom, there's your entire project management and collaboration application. Tickets, wiki, chat, notes, forum, rss feeds for updates.

## differences

There are a few conceptual differences between fossil and git.

### Distinct repositorys and checkouts

Fossil conceptualizes repositories as separate from checkouts, whereas git comingles them: in git, the repo (the `.git/` directory) is included when you check out the code. In fossil, the repo (the sqlite database) is not necessarily included in the checkout. The implication is that you can `open` the repo as many times as you want into different directories. Once for each branch, for example. (`cd dev && fossil open ../my-repo.fossil`) This allows you to quickly `cd` from branch to branch without invalidating build objects or stopping processes like you have to with git.

### No rewriting history

Fossil has no rebasing and no squashing commits. The call this recording "what you did", as opposed to "what you wish you had done". This frightens me a little bit because I really like git's ability to let you clean up, reorder, and squash commits before merging. Allegedly, fossil has superior reporting tools (which I believe since it is build on sqlite) that make this acceptable.

### Other

Other smaller differences:

- Undetachable Head: There is no *detached head* state in fossil thanks to the way sqlite works

- The world is (not) a stage: Fossil has no concept of a staging area.

- As needed branches: Do a bunch of work, and then on first commit, create the branch *as needed*: `fossil commit --branch my-branch`

- autosync: push to remote on every commit. Get remote updates on every `fossil update`

- migration friendly: you can import from and export to git and svn.

## Resources

- Fossil: <https://fossil-scm.org/home/doc/trunk/www/index.wiki>
