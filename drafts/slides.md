---
title: slides
subtitle: a practical guide to creating presentations with pandoc and reveal.js
created_at: 2022-02-15
---

## Contents

* [Introduction](#introduction)
* [Pandoc](#pandoc)
* [Caveats](#caveats)
  * [Nested slides](#nested-slides)
  * [Speaker notes](#speaker-notes)
  * [CDN](#cdn)
  * [Fragments](#fragments)
* [Deployment](#deployment)
* [Conclusion](#conclusion)
* [Resources](#resources)

## Introduction

For a long time now, my go-to framework for making slides[^slides] has been reveal.js because you can write markdown and export HTML. It's portable plain text that works great, and is surprisingly robust, with transitions and animations and speaker notes and everything.

[^slides]: or "making a deck", or making a presentation, or making a slideshow. Prezi? Prezo? Whatever you want to call it.

I find it much more convenient than using something like PowerPoint or even Google Slides.

Actually using it though has historically been kind of cumbersome for me. My process would be to clone the repo, delete all the examples and documentation, and then start writing my slides. And then starting an http server when I want to present.

## Pandoc

But then I found out you can just `pandoc -t revealjs` a markdown file and *not* do all that other stuff.

Glorious!

## Caveats

Look, here's the thing. You're meant to create reveal.js slides in HTML. Markdown is supported through a plugin which brings some of its own syntax. Whereas pandoc has to (chooses to?) support converting markdown to like five different slide / presentation frameworks. So it introduces some of its own special syntax to address what I'm sure are no small number of idiosyncrasies.

Thus, a few caveats.

### Nested slides

For some reason pandoc doesn't support this out of the box.[^nesting]

[^nesting]: I mean, it kind of does. But only, I think, if you if you use first and second level headings. Which I don't really like doing. I like to reserve h1 for the page (slide) title, and then use h2s to represent the top level logical sections in my documents.

You can fix it though by applying the `--slide-level=n` flag to your pandoc command, where `n` is the level of the header that you want to be nested. e.g., I use `--slide-level=3` so that h3 elements become slides nested under h2 elements.

So for example, in this document itself, my markdown looks like this:

```
## Limitations

There are....

### Nested slides

For some...
```

If I were to turn this document into a reveal.js presentation, I would run `pandoc -t revealjs -o slides.html -s --slide-level=3 slides.md`

Explanation:

- `pandoc`: invoke pandoc
- `-t revealjs`: convert the source to reveal.js output
- `-o slides.html`: save the output as "slides.html"
- `-s`: create the output as a standalone document (required for exporting a slideshow)
- `--slide-level=3`: as described above
- `slides.md`: the source file to convert

### Speaker notes

Reveal.js has a great "speaker view" pop-out window that shows you a timer, the next slide, and speaker notes for the current slide.

My presentation style tends toward the Takahashi Method. (Very little content on each slide. Likely 3 - 5 words, or a single image.) So I rely heavily on speaker notes to be sure I hit each of the talking points I want to.

The reveal.js markdown plugin allows you to use a very simple syntax:

```
## Title

bla bla bla

Note:
This only appears in speaker view
```

But pandoc doesn't support this. It will include the note as text in the slide.

Okay, no problem, the canonical way to include notes in reveal.js is to place them inside an `<aside>` tag.

And this is what the documentation recommends on the pandoc site.

```
## Title

bla bla bla

<aside class="notes">
This only appears in speaker view
</aside>
```

And this does work.

But then, the man page suggests a syntax that the online docs don't:

```
## Title

bla bla bla

::: notes
This only appears in speaker view
:::
```

### CDN

I'll slip this little caveat in here real quickly: pandoc will by default attempt to download the reveal.js library from a CDN.

If you want to work offline or for some other reason want to cache the library, then you can download it somewhere and include something like `--revealjs-url="~/lib/reveal/reveal.js"` in the command arguments.

### Fragments

reveal.js has a concept of "fragments" which enter the slide incrementally.

With pandoc, you can globally turn fragments on for all list item elements with the `pandoc --incremental` flag. Or case-by-case:

```
::: incremental
- these
- fly in
- one by one
- on advance
:::

::: nonincremental
- these
- all show up
- at the same time
:::
```

## Deployment

I went ahead and copied a couple of my talks into a repo, and wrote a little `gitlab-ci.yml` to deploy them to gitlab pages on push.

- repo: <https://gitlab.com/chrismanbrown/slides/>
- www: <https://chrismanbrown.gitlab.io/slides/>

It required a little bit of syntax updating, but I think it is overall more simple, and worth it.

## Conclusion

For any limitations, this is actually a great improvement over using reveal.js directly because it radically cuts down on boilerplate and setup. It allows me to just start writing markdown and organizing my thoughts sequentially and hierarchically.

The best tools for thinking are the ones that just get out of your way.

## Resources

- reveal.js: <https://github.com/hakimel/reveal.js/>
- Takahashi Method: <https://en.wikipedia.org/wiki/Takahashi_method>
- *DevOps for Presentations: Reveal.js, Markdown, Pandoc, GitLab CI*, Benji Fisher: <https://medium.com/isovera/devops-for-presentations-reveal-js-markdown-pandoc-gitlab-ci-34d07d2c1011>
