---
title: podman
subtitle: containers without docker
created_at: 2022-02-20
---

## Motivation

Docker is cool. It popularized containers for the masses.

From the beginning though, there were a few things I didn't like about it:

1. Requires root access
2. Runs as a daemon

And when they changed their pricing model, I think a lot of teams and companies started looking at alternatives. (My company, e.g., is at the moment encouraging all of us to consider open source alternatives to Docker Desktop.)

## What is podman

Podman is a container management tool from Redhat.

It daemonless and rootless, and because it is <abbr title="Open Container Initiative">OCI</abbr> compliant just like Docker, you can `alias docker=podman` and seamlessly start using it.

## Getting Started

Install, init, and start:

- `brew install docker`
- `podman machine init`
- `podman machine start`

Add some aliases:

- `alias docker=podman`
- `alias pod=podman` (NOTE: don't do this if you use CocoaPods :P)

## Compose

You can install the containers group's podman-compose as an alternative to docker-compose:

- `pip3 install podman-compose`

## Desktop

There is not a great replacement for docker desktop, but [podman-tui](https://github.com/containers/podman-tui) looks really nice and feature complete.

## Resources

- podman: <https://podman.io/>
- podman-compose: <https://github.com/containers/podman-compose>
- podman-tui: <https://github.com/containers/podman-tui>
