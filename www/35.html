<html lang="en">
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta charset="utf-8">
<title>
chrismanbrown
</title>
<meta name="author" content="Chrisman Brown">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="module" src="scripts/themes.js"></script>
<link rel="alternate" type="application/rss+xml" href="/rss.xml" title="chrisman">
<link rel="stylesheet" href="styles/reset.css">
<link rel="stylesheet" href="styles/main.css">
<link rel="stylesheet" href="styles/code.css">
<link rel="index" href="/list" /> <link rel="prev" href="/34">
<link rel="next" href="/36">
</head>
<body>
<header>
👩‍💻 chrismanbrown.gitlab.io
<nav>
<a href="about.html">About</a> · <a href="list.html">Writing</a> · <a
href="wiki/Home.html">Wiki</a> · <a href="contact.html">Contact</a> · <a
href="feeds.html">Feeds</a> · <a href="uses.html">Uses</a> · <a
href="hire.html">Careers</a>
</nav>
</header>
<main>
<h1 class="title">
Lets Read: Structure and Interpretation of Computer Programs
</h1>
<p>
Chapter 2: Part 1
</p>
<p>
2022-02-22
</p>
<p>This is the fourth entry in the <em>Let’s Read
<abbr title="Structure and Interpretation of Computer Programs">SICP</em></abbr>
series.</p>
<p>« <a href="34.html">previous</a> | <a href="31.html">index</a></p>
<hr />
<h2 id="building-abstractions-with-data">2. Building Abstractions with
Data</h2>
<p>Okay we just wrapped up creating abstractions with procedures, now
it’s time to talk about data!</p>
<p>I can’t say too much about the chapter intro. It mostly just
described things that we’re going to learn that which, not having
learned them yet, did not mean too much to me.</p>
<h2 id="introduction-to-data-abstraction">2.1 Introduction to Data
Abstraction</h2>
<p>Data abstractions have <em>constructors</em>, which are familiar from
<abbr title="object oriented programming">OOP</abbr> land. It’s how you
make a thing.</p>
<p>And they also have <em>selectors</em>, like “getters”, which is how
you extract data from the abstraction.</p>
<p>I find myself once again considering functional programming
vs. object-oriented programming because to me, these two terms are
deeply entrenched in
<abbr title="Object Oriented Programming">OOP.</abbr> But this book for
the most part seems to avoid discussing either paradigm. I’ve already
tempered a lot of my “Functional good, Object Oriented bad” feelings and
rhetoric over the years, and this omission makes me wonder how
superficial that divide really may be.</p>
<h2 id="example-arithmetic-operations-for-rational-numbers">2.1.1
Example: Arithmetic Operations for Rational Numbers</h2>
<p>Our introduction to data abstractions is rational numbers, aka
fractions.</p>
<p>The authors do a good job of showing how the abstraction allows us to
separate use of an abstraction from the implementation of an abstraction
by not showing us the implementation at all.</p>
<p>Constructor signature:
<code>(make-rational-number &lt;n&gt; &lt;d&gt;)</code>. Takes an
integer numerator and an integer denominator and returns a rational
number.</p>
<p>Selector signatures:</p>
<ul>
<li><p><code>(numerator &lt;x&gt;)</code>. Takes a rational number and
returns an integer numerator.</p></li>
<li><p><code>(denominator &lt;x&gt;)</code>. Takes a rational number and
returns an integer denominator.</p></li>
</ul>
<p>This is enough to get started writing procedures for adding,
subtracting, multiplying, and dividing rational numbers without ever
knowing (or caring) about how the constructor or selectors are
implemented.</p>
<p>The implementation is something I <em>do</em> care about though
because they mark the appearance of some lisp celebrities.</p>
<p>Constructor<a href="#fn1" class="footnote-ref" id="fnref1"
role="doc-noteref"><sup>1</sup></a>:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode scheme"><code class="sourceCode scheme"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> make-rational-number </span><span class="kw">cons</span>)</span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> numerator </span><span class="kw">car</span>)</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> denominator </span><span class="kw">cdr</span>)</span></code></pre></div>
<h2 id="abstraction-barriers">2.1.2 Abstraction Barriers</h2>
<p>Data abstraction allows you to create abstraction barriers.</p>
<p>Each box in the following diagram represents a barrier between
different “levels” of the program.</p>
<figure>
<img src="img/sicp-2-1-1.mermaid.svg" alt="diagram" />
<figcaption aria-hidden="true">diagram</figcaption>
</figure>
<p>Being a program that uses rational numbers in the problem domain, you
have access to public methods like <code>add-rat</code>,
<code>sub-rat</code>, <code>rat-eql?</code>, etc.</p>
<p>You do not have access to anything below that barrier.</p>
<p>Those public methods have access to the constructor and selector
procedures, <code>make-rat</code>, <code>numer</code>, and
<code>denom</code>. They do not have knowledge of how those methods are
implemented.</p>
<p>The constructor and selectors, however, know that the data is
represented by pairs using <code>cons</code>, <code>car</code>,
<code>cdr</code>.</p>
<p>And those language primitives know how they are implemented in the
language.</p>
<p>Abstraction barriers allow us to freely change implementation details
as long as we keep the APIs the same.</p>
<h2 id="what-is-meant-by-data">2.1.3 What Is Meant by Data?</h2>
<p>A constructor, some selectors, and a definition.</p>
<p>For example, a pair: for any objects <code>x</code> and
<code>y</code>, if <code>z</code> is <code>(cons x y)</code> then
<code>(car z)</code> is <code>x</code> and <code>(cdr z)</code> is
<code>y</code>.</p>
<p>So that’s data.</p>
<p>Then this short section does something kind of unexpected.</p>
<p>It demonstrates an implementation of pair:</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode scheme"><code class="sourceCode scheme"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> </span>(<span class="kw">cons</span> x y)</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>  (<span class="ex">define</span><span class="fu"> </span>(dispatch m)</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>    (<span class="kw">cond</span> ((<span class="op">=</span> m <span class="dv">0</span>) x)</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>          ((<span class="op">=</span> m <span class="dv">1</span>) y)</span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a>          (<span class="kw">else</span> </span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a>           (<span class="kw">error</span> <span class="st">&quot;Argument not 0 or 1:</span></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a><span class="st">                   CONS&quot;</span> m))))</span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a>  dispatch)</span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-10"><a href="#cb2-10" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> </span>(<span class="kw">car</span> z) (z <span class="dv">0</span>))</span>
<span id="cb2-11"><a href="#cb2-11" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> </span>(<span class="kw">cdr</span> z) (z <span class="dv">1</span>))</span></code></pre></div>
<p>…which is not itself unexpected.</p>
<p>What is unexpected is that the authors then tell us:</p>
<blockquote>
<p>This use of procedures corresponds to nothing like our intuitive
notion of what data should be.</p>
</blockquote>
<p>To which I say… why would you think that?</p>
<p>We just spent the last section making a constructor and selectors for
rational numbers out of procedures. How is this any different? I see
passing data to procedure, and then returning data from a procedure.</p>
<p>The only thing I see here that is kind of interesting is that I think
this might be the first time the authors have shown a
<em>closure</em>.</p>
<p>When calling the constructor <code>cons</code>, you pass two
arguments <code>x</code> and <code>y</code> and then return a new
procedure <code>dispatch</code> that <em>closes over</em> and retains
access to the values of <code>x</code> and <code>y</code>.</p>
<p>But they make no mention of that.</p>
<p>They do say that even though this is a “procedural implementation” of
pairs and is a valid implementation, it is not a “real” data structure.
Which I guess is true in the sense that it is not a primitive data
structure like a list or a hash or a tuple or something.</p>
<p>Finally:</p>
<blockquote>
<p>This example also demonstrates that the ability to manipulate
procedures as objects automatically provides the ability to represent
compound data. This may seem a curiosity now, but procedural
representations of data will play a central role in our programming
repertoire. This style of programming is often called <em>message
passing</em>, and we will be using it as a basic tool in Chapter 3 when
we address the issues of modeling and simulation.</p>
</blockquote>
<p>I think my inability to understand why procedural data structures are
super special is also preventing me from understanding what <em>message
passing</em> is in this context, but I guess I’ll just wait until
Chapter 3 to find out more about that!</p>
<h2 id="extended-exercise-interval-arithmetic">2.1.4 Extended Exercise:
Interval Arithmetic</h2>
<p>Interval arithmetic such as when dealing with resistors that, e.g.,
have a plus or minus 5% tolerance.</p>
<p>This can be represented also as a pair which stores the minimum and
the maximum resistance. The example goes on to show that you can have
different APIs for the same data (min/max vs. center + width vs. center
+ %) and that using different formulas can produce different results
even though the formulas are equal. (Weird math!)</p>
<h2 id="hierarchical-data-and-the-closure-property">2.2 Hierarchical
Data and the Closure Property</h2>
<p>This section introduces <em>box and pointer</em> notation.</p>
<p>Closure is not what you think it is: closure is the property that
allows combinations of things to be combined into combinations of
things.</p>
<p>I think we’re going to use <code>cons</code> pairs to make all of the
following data types.</p>
<h2 id="representing-sequences">2.2.1 Representing Sequences</h2>
<p>All about lists! Filter em, map em, stick em in a stew!</p>
<p>One thing I learned is that scheme’s <code>map</code> procedure takes
a procedure and n number of lists, and then sequentially applies the
procedure to the ith number of each list!</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode scheme"><code class="sourceCode scheme"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a>(map <span class="op">+</span> &#39;(<span class="dv">1</span> <span class="dv">2</span> <span class="dv">3</span>) &#39;(<span class="dv">4</span> <span class="dv">5</span> <span class="dv">6</span>) &#39;(<span class="dv">7</span> <span class="dv">8</span> <span class="dv">9</span>))</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="co">; -&gt; (12 15 18)</span></span></code></pre></div>
<h2 id="hierarchical-structures">2.2.2 Hierarchical Structures</h2>
<p>A list comprised of other lists can be thought of as a tree.</p>
<p>Look at the list <code>((1 2) 3 4)</code></p>
<p>It can be represented as a tree:</p>
<figure>
<img src="img/sicp-2-1-2.mermaid.svg" alt="diagram" />
<figcaption aria-hidden="true">diagram</figcaption>
</figure>
<p>The length of the list is different to the number of leaves on the
tree.</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode scheme"><code class="sourceCode scheme"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> x </span>(<span class="kw">cons</span> (<span class="kw">list</span> <span class="dv">1</span> <span class="dv">2</span>) (<span class="kw">list</span> <span class="dv">3</span> <span class="dv">4</span>)))</span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a>(<span class="kw">length</span> x)       <span class="co">; -&gt; 3</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>(count-leaves x) <span class="co">; -&gt; 4</span></span></code></pre></div>
<p><code>count-leaves</code> introduces basic tree iteration:</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode scheme"><code class="sourceCode scheme"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> </span>(count-leaves t)</span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a>  (<span class="kw">cond</span> ((<span class="kw">null?</span> t) <span class="dv">0</span>)</span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a>        (<span class="kw">not</span> (<span class="kw">pair?</span> t) <span class="dv">1</span>)</span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a>        (<span class="kw">else</span> (<span class="op">+</span> (count-leaves (<span class="kw">car</span> t))</span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a>                 (count-leaves (<span class="kw">cdr</span> t))))))</span></code></pre></div>
<h2 id="sequences-as-conventional-interfaces">2.2.3 Sequences as
Conventional Interfaces</h2>
<p>Okay this gets into the kind of functional, list processing
programming that I really like.</p>
<p>The authors introduce two arbitrary procedures. The first sums the
squares of all the odd numbers in a tree. The second describes a
procedure <em>f(n)</em> that produces a list of all of the even
Fibonacci numbers <em>Fib(k)</em> where <code>k &lt; n</code>.</p>
<p>They implement the procedures in a very imperative way that makes
them structurally look quite different from each other.</p>
<p>But when described as <em>signal flows</em> they look like they
should be more similar.</p>
<figure>
<img src="img/sicp-2-1-3.mermaid.svg" alt="diagram" />
<figcaption aria-hidden="true">diagram</figcaption>
</figure>
<p>So we create a bunch of accumulators, maps, filters, and enumerators,
and suddenly everything gets a lot more <em>functional</em>, modular,
composable, etc.</p>
<h3 id="nested-mappings">Nested Mappings</h3>
<p>Okay this is neat, too.</p>
<p>Imagine a situation that would call for a nested for loop. (The given
example is a procedure <em>f(n)</em> that returns all triples <em>(i, j,
i + j)</em> where <code>1 &lt;= j &lt;= i &lt;= n</code>, where
<code>i + j</code> is prime.</p>
<p>So you need to iterate over <code>i</code> for each
<code>1..n</code>, and at each step iterate over <code>j</code> for each
<code>1..i</code>, and accumulate <code>(i j)</code> if
<code>i + j</code> is prime.</p>
<p>In order to create this process, we first create a procedure using
our enumerator, accumulator, and map from earlier in this section.</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode scheme"><code class="sourceCode scheme"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> </span>(make-pairs n)</span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a>  (accumulate</span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a>    <span class="kw">append</span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a>    nil</span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a>    (map (<span class="kw">lambda</span> (i)</span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a>      (map (<span class="kw">lambda</span> (j)</span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a>        (<span class="kw">list</span> i j))</span>
<span id="cb6-8"><a href="#cb6-8" aria-hidden="true" tabindex="-1"></a>        (enumerate-interval <span class="dv">1</span> (<span class="op">-</span> i <span class="dv">1</span>))))</span>
<span id="cb6-9"><a href="#cb6-9" aria-hidden="true" tabindex="-1"></a>      (enumerate-interval <span class="dv">1</span> n))))</span>
<span id="cb6-10"><a href="#cb6-10" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb6-11"><a href="#cb6-11" aria-hidden="true" tabindex="-1"></a>(make-pairs <span class="dv">4</span>)</span>
<span id="cb6-12"><a href="#cb6-12" aria-hidden="true" tabindex="-1"></a><span class="co">;; -&gt; ((2 1) (3 1) (3 2) (4 1) (4 2) (4 3) (5 1) (5 2) (5 3) (5 4))</span></span></code></pre></div>
<p>This <em>accumulate append nil</em> pattern is very common, and is in
fact the implementation of <em>flatmap</em>, whom I know from my forays
into functional programming<a href="#fn2" class="footnote-ref"
id="fnref2" role="doc-noteref"><sup>2</sup></a>:</p>
<div class="sourceCode" id="cb7"><pre
class="sourceCode scheme"><code class="sourceCode scheme"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a>(<span class="ex">define</span><span class="fu"> </span>(flatmap proc seq)</span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a>  (accumulate <span class="kw">append</span> nil (map proc seq)))</span></code></pre></div>
<p>They go on to show how flatmap can help us compute all the solutions
to the Eight Queens puzzle<a href="#fn3" class="footnote-ref"
id="fnref3" role="doc-noteref"><sup>3</sup></a>.</p>
<h2 id="example-a-picture-language">2.2.4 Example: A Picture
Language</h2>
<p>This section was good but ultimately disappointing because I thought
we were going to be writing an actual graphics program. Instead, the
content was purely theoretical, focusing on a closed (as in implements
the closure property) procedural data structure called a
<em>painter</em> that we can combine and transform in lots of different
ways to create mosaics.</p>
<p>It was straightforward enough until it got to how frames are
implemented using three vectors (an origin and two edges) and a painter
can be applied to a frame using a <em>frame coordinate map</em>. The
implications were clear enough: you can trivially flip, invert, distort,
and otherwise transform a frame (and thus the picture created by the
painter) by setting different origins and edges. But I didn’t understand
the implementation details.</p>
<h2 id="summary">Summary</h2>
<p>So far, Chapter 2 is overall a little more chill than Chapter 1 was,
as far as complicated math goes.</p>
<p>I think the problems with Chapter 1 come from the fact that it was
trying to express complicated ideas about procedures as abstraction
while using no data abstractions. So if you’re trying to express
complicated ideas with nothing but primitive numbers, then you’re
probably pretty likely to get fairly fancy with those numbers. Now that
we’re dealing with procedural abstractions <em>and</em> data
abstractions, our subject matter by itself is complicated enough that we
can do some really interesting things without having to resort to
complex mathematical formulas and theorems.</p>
<p>Okay, there are three more sections left in Chapter 2. Stay
tuned!</p>
</main>
<footer>
<p>
:wq
</p>
<div style="text-align: center; margin: 2rem 0;">
𐡸 𐡸 𐡸
</div>
<p><a href="mailto:christopher.p.brown@gmail.com?subject=RE: Lets Read: Structure and Interpretation of Computer Programs">Reply
by Email</a></p>
<p>
Filed under:
</p>
<ul>
<li>
<a href="t-complexity.html">complexity</a>
</li>
<li>
<a href="t-sicp.html">sicp</a>
</li>
<li>
<a href="t-lets-read.html">lets-read</a>
</li>
<li>
<a href="t-book.html">book</a>
</li>
</ul>
<p>
<a href="34.html">« Previous</a> | <a href="35.html">2022-02-22</a> |
<a href="36.html">Next »</a>
</p>
</footer>
</body>
</html>
<aside id="footnotes" class="footnotes footnotes-end-of-document"
role="doc-endnotes">
<hr />
<ol>
<li id="fn1"><p>The property that allows me to write in this style is
called <em>referential transparency</em>. You <em>could</em> write
<code>make-rational-number</code> as
<code>(define (mak-rat x y) (cons x y))</code> but, what’s the point?
You’re just handing the arguments from one procedure directly to another
without doing anything with them. Might as well cut out the middle man
as it were and just make the constructor a variable, an assignment,
instead of a procedure. You don’t need to <em>do</em> anything with
<code>x</code> and <code>y</code>. So just say that
<code>make-rational-number</code> <em>is</em> cons, not that it does
something <em>with</em> cons. I think this is also somehow related to
point-free programming (not to be confused with <em>pointless</em>
programming, which is something else entirely), which is when you write
a procedure that makes no reference to its arguments. Anyway, the
authors recommend against this style of writing because they say it
makes it difficult to debug because it destroys the stack trace. Which
is legit, I guess. But I don’t care about being able to debug at this
elementary stage of adding and subtracting fractions.<a href="#fnref1"
class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn2"><p>When I first met <em>flatmap</em> deep in see Chapter 9
of <em>Professor Frisby’s Mostly Adequate Guide to Functional
Programming in JavaScript</em>, it seemed very mysterious surrounded by
all those monads and functors and stuff. SICP’s description here is much
more simple and straightforward. See: <a
href="https://mostly-adequate.gitbook.io/mostly-adequate-guide/ch09#my-chain-hits-my-chest"
class="uri">https://mostly-adequate.gitbook.io/mostly-adequate-guide/ch09#my-chain-hits-my-chest</a><a
href="#fnref2" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
<li id="fn3"><p>See: <a
href="https://en.wikipedia.org/wiki/Eight_queens_puzzle"
class="uri">https://en.wikipedia.org/wiki/Eight_queens_puzzle</a><a
href="#fnref3" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</aside>
