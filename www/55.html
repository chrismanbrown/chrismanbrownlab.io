<html lang="en">
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta charset="utf-8">
<title>
chrismanbrown
</title>
<meta name="author" content="Chrisman Brown">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="module" src="scripts/themes.js"></script>
<link rel="alternate" type="application/rss+xml" href="/rss.xml" title="chrisman">
<link rel="stylesheet" href="styles/reset.css">
<link rel="stylesheet" href="styles/main.css">
<link rel="stylesheet" href="styles/code.css">
<link rel="index" href="/list" /> <link rel="prev" href="/54">
<link rel="next" href="/56">
</head>
<body>
<header>
👩‍💻 chrismanbrown.gitlab.io
<nav>
<a href="about.html">About</a> · <a href="list.html">Writing</a> · <a
href="wiki/Home.html">Wiki</a> · <a href="contact.html">Contact</a> · <a
href="feeds.html">Feeds</a> · <a href="uses.html">Uses</a> · <a
href="hire.html">Careers</a>
</nav>
</header>
<main>
<h1 class="title">
Incremental Databases
</h1>
<p>
Evolving data from plain text logs, to recfiles, and finally to sqlite
</p>
<p>
2024-09-07
</p>
<h2 id="contents">Contents</h2>
<ol type="1">
<li><a href="#phase-1-lines-of-text">Phase 1: Lines of text</a></li>
<li><a href="#phase-2-tabular-data">Phase 2: Tabular Data</a></li>
<li><a href="#interlude-a-client">Interlude: A client</a></li>
<li><a href="#phase-3-data-integrity">Phase 3: Data Integrity</a></li>
<li><a href="#phase-4-too-big-for-rec">Phase 4: Too big for rec</a></li>
<li><a href="#conclusion">Conclusion</a></li>
<li><a href="#resources">Resources</a></li>
</ol>
<h2 id="phase-1-lines-of-text">Phase 1: Lines of text</h2>
<p>The most simple database I use on the regular (that isn’t the
filesystem) is a plain log of lines of text. This is how my todo list
and my daily log work. This is really great as long as your data isn’t
really that tabular.</p>
<p>In fact, I actually have a microblog powered by a textfile formatted
this way. It’s just a timestamp followed by some text. It gets turned
into <abbr title="Hypertext Machine Learning">html</abbr> and
<abbr title="Really Special Syndication">rss</abbr> and published, and
that’s all I need for it.</p>
<p>Contrast this with my todo.txt, which has a lot of syntax:</p>
<figure>
<pre><code>x get a housewarming gift for hoodzilla +shopping
. book a hotel for the convention +travel P2
. 2024-09-06 can i get a discount on this bill if i ask nicely? +bills
x play D&amp;D with friends
x 2024-09-03 2024-09-02 email the guy about the thing +project P1</code></pre>
<figcaption>
figure: a few random and slightly edited lines from todo.txt
</figcaption>
</figure>
<p>I can view all my pending todos with a
<code>grep '^\. ' todo.txt</code>.</p>
<p>Pros: Extremely easy to append a new line</p>
<p>Cons: Relies on “magical” knowledge of the format to do some queries.
e.g. status characters are always at the beginning of the line, followed
by an optional created-on date. Unless the item is completed, at which
point it is followed by a completed-on date and an optional created-on
date. This structure exists nowhere except in my head!</p>
<h2 id="phase-2-tabular-data">Phase 2: Tabular Data</h2>
<p>My todo.txt syntax has evolved organically over time in order to make
it convenient to search and query. Each item starts with a single
character denoting its status. A dot for pending, an x for completed.
Lines can have a created-on date and a completed-on date. It can have
tags for context, represented a plus sign followed by a word. It can
have a priority, denoted by P1, P2, P3.</p>
<p>This is still perfectly usable as is, and I can get all the
particular information I want from it with a little grep and
<abbr title="Aho Weinberger Kernighan">awk.</abbr> But it is also
becoming very tabular at this point: it can be represented by keys and
fields.</p>
<p>This is the point of data evolution at which I might consider
creating a recfile.</p>
<figure>
<pre><code>status: complete
text: get a housewarming gift for hoodzilla
tags: shopping

status: todo
text: book a hotel for the convention
tags: travel
priorty: P2

status: todo
created_on: 2024-09-06
text: can i get a discount on this bill if i ask nicely?
tags: bills

status: complete
text: play D&amp;D with friends

status: complete
completed_on: 2024-09-03
created_on: 2024-09-02
text: email the guy about the thing
tags: project
priorty: P1</code></pre>
<figcaption>
figure: todo.txt but a recfile
</figcaption>
</figure>
<p>Pros: Still very human readable, human writable. Highly queryable.
Highly structured. No more “magical” structure.</p>
<p>Cons: More verbose. Can no longer update by appending a single
line.</p>
<p>At this stage, I’d be happy with either a log or a recfile. The
reason I’d want to advance to a recfile is if I want more complex
querying and reporting. e.g. aggregate functions or grouping by values,
etc.</p>
<h2 id="interlude-a-client">Interlude: A client</h2>
<p>I have a todo.sh script where I’ve been collecting all my queries:
add a new item, mark an item as complete. List all pending, list all
completed, list by priority, list by tag, etc.</p>
<p>This creates an abstraction called an “interface” between behavior
(e.g. add a new todo item) and its implementation (e.g. format the
string and prepend it to todo.txt). This is convenient because A) it
allows for shorthand syntax: I can capture a new todo item by typing “t
a something i really need to do” (where “t a” is short for “todo add”);
and B) it allows me to change the implementation logic or the
representation of the data behind the scenes without changing how I
interact with the list at the interface level. It could be greps and
awks, or it could be recsels. It doesn’t matter.</p>
<p>This mostly addresses the cons of a recfile listed above. I can still
“t a some item +context P1” and parse that string and insert it into a
recfile.</p>
<h2 id="phase-3-data-integrity">Phase 3: Data Integrity</h2>
<p>At this point the data is still very freeform. It is without a
schema. I can just slap a new record in there with a text editor. I can
add, remove, and update fields all willy-nilly. Maybe I want to create a
brand new field for due dates. There are no rules. The limit does not
exist!</p>
<p>The next phase in this incremental database journey is to clean up
this lawless town. To add a schema and start enforcing types and
restrictions. recutils support this via record descriptors.</p>
<p>I could describe my todo records thusly:</p>
<figure>
<pre><code>%rec: todo
%doc: an item on my todo list
%key: id
%type: id int
%auto: id
%type: created,completed,due date
%auto: created
%type: status enum complete todo blocked deferred delegated note
%type: priority enum 1 2 3 0
%mandatory: id created status text
%allowed: id created completed due status text tag priority

id: 0
created_on: 2024-08-26
completed_on: 2024-09-02
status: complete
text: get a housewarming gift for hoodzilla
tags: shopping

...</code></pre>
<figcaption>
figure: a record descriptor and a record from todo.rec
</figcaption>
</figure>
<p>Now I can still just edit the todo list manually if I want to. (And I
will if I want to edit or rephrase the text of an item.) But I’m
primarily using an abstraction, todo.sh, to interact with it. So I can
start using recins(1) to add items, which will start to enforce the
schema: autoinserting an ‘id’ and a ‘created,’ only allowing a status
and a priority if the values are present in the enumeration. Rejecting
any field that is not present in ‘%allowed’, be it a typo or whatever.
Refusing to insert without all of the mandatory fields.</p>
<p>Now we have data integrity.</p>
<p>And I can run some queries a little bit more easily than with a bunch
of awk.</p>
<h2 id="phase-4-too-big-for-rec">Phase 4: Too big for rec</h2>
<p>recutils is optimized for small data. My todo list is plenty small
enough for it. But a bigger dataset, like my goodreads.com reading
history, is way too big for it.</p>
<p>I have about 700 books in this database. recsel can read quickly
enough. But recins and recset are incredibly slow writing to the
database.</p>
<p>Fortunately, it is very easy to outgrow recutils:</p>
<figure>
<pre><code>&gt; recsel books.rec | rec2csv &gt; books.csv
&gt; sqlite3 books.sqlite &quot;.import --csv books.csv books&quot;
&gt; # test the import with a query:
&gt; sqlite3 books.sqlite &quot;select Title from books order by random() limit 3&quot;
Good Omens: The Nice and Accurate Prophecies of Agnes Nutter, Witch
Half Empty
A Heartbreaking Work of Staggering Genius</code></pre>
<figcaption>
figure: exporting data from rec database as a
<abbr title="Comma Separated File">csv</abbr> and importing it into a
sqlite database
</figcaption>
</figure>
<p>That’s it! Super quick, super easy. That’s all you need to get your
data into a sql database.</p>
<p>The thing about sql is that it is relational. Recfiles <em>can</em>
be relational. But I don’t often use them that way. In fact if I’m doing
a bunch of joins on different recfiles, that’s a sign to me that my data
is relational enough to deserve at least considering migrating to sql.
Either way, in my experience the data probably wants a little
normalization at this point to prepare it for its new relational
life.</p>
<p>If you wanted to be fancy, you could probably print the record
descriptor with ‘recinf -d books.rec’ and generate a schema with some
kind of script. Here is a proof-of-concept awk script that accomplished
this:</p>
<figure>
<pre><code>#!/bin/awk -f
# rec2schema
# usage: recinf -d db.rec | rec2schema

$1 ~ &quot;%rec:&quot; {
  rec=$2
}

$1 ~ &quot;%allowed:&quot; {
  for(i=2;i&lt;=NF;i++)
    column[$i] = $i
  next
}

$1 ~ &quot;%type:&quot; &amp;&amp; $3 ~ &quot;enum&quot; {
  type[$2] = &quot;text&quot;
  opts[$2] = opts[$2] &quot; check( &quot; $2 &quot; in (&quot;
  for (i=4;i&lt;=NF;i++) {
    opts[$2] = opts[$2] &quot; &#39;&quot; $i &quot;&#39;&quot;
    if (i != NF) opts[$2] = opts[$2] &quot;,&quot;
  }
  opts[$2] = opts[$2] &quot;) )&quot;
  next
}

$1 ~ &quot;%type:&quot; {
  split($2,types,&quot;,&quot;)
  for(t in types) {
    k=types[t]
    type[k] = $3 
  }
  next
}

$1 ~ &quot;%key:&quot; {
  opts[$2] = opts[$2] &quot; primary key&quot;
  next
}

$1 ~ &quot;%mandatory:&quot; {
  opts[$2] = opts[$2] &quot; not null&quot;
  next
}

$1 ~ &quot;%auto&quot; {
  if (type[$2] == &quot;date&quot;) {
    opts[$2] = opts[$2] &quot; default current_timestamp&quot;
  } else {
    opts[$2] = opts[$2] &quot; autoincrement&quot;
  }
  next
}

END {
  print &quot;create table if not exists &quot; rec &quot; (&quot;
  for (c in column) {
    t = type[c] == &quot;&quot; ? &quot;text&quot; :
      type[c] == &quot;date&quot; ? &quot;timestamp&quot; :
      type[c] == &quot;line&quot; ? &quot;text&quot; :
      type[c] == &quot;int&quot; ? &quot;integer&quot; :
      type[c]
    printf &quot;%s %s%s,\n&quot;,c,t,opts[c]
  }
  print &quot;);&quot;
}</code></pre>
<figcaption>
figure: rec2schema.awk
</figcaption>
</figure>
<p>This generates something like this from todo.rec:</p>
<figure>
<pre><code>create table if not exists todo (
id integer primary key autoincrement not null,
created timestamp default current_timestamp,
completed timestamp,
due timestamp,
text text,
tag text,
status text check( status in ( &#39;complete&#39;, &#39;todo&#39;, &#39;blocked&#39;, &#39;deferred&#39;, &#39;delegated&#39;, &#39;note&#39;) ),
priority text check( priority in ( &#39;1&#39;, &#39;2&#39;, &#39;3&#39;, &#39;0&#39;) ),
);</code></pre>
<figcaption>
figure: an sql Create Table statement generated from a record descriptor
with rec2schema.awk
</figcaption>
</figure>
<p>You will probably still need to tweak the output. e.g. enum types
should probably be references to a separate enumeration table. And rec
types should definitely get turned into foreign keys.</p>
<figure>
<pre><code>&gt; recsel books.rec | rec2csv &gt; books.csv
&gt; recinf -d books.rec | rec2schema &gt; books.schema
&gt; # (edit books.schema as needed)
&gt; printf &quot;%s\n&quot; \
    &quot;.read books.schema&quot; \
    &quot;.import --csv --skip 1 books.csv books&quot; \
    | books.sqlite</code></pre>
<figcaption>
figure: importing rows and a schema into a sqlite table from a recfile
</figcaption>
</figure>
<p>Anyway, the point is that it’s really easy to move data from rec to
sqlite.</p>
<p>Pros: Blazingly Fast. All the expressiveness of SQL.</p>
<p>Cons: No longer human readable or writable.</p>
<p>Aside: did you know that sqlite can answer queries in markdown
format?</p>
<p>This query:</p>
<figure>
<pre><code>&gt; printf &quot;%s\n&quot; \
  &quot;.mode markdown&quot; \
  &quot;select Title,Author,Format,Shelf \
    from books b \
    join authors a on b.AuthorId = a.Id \
    join exclusive_shelves e on b.ExclusiveShelfId = e.Id \
    order by b.Id desc \
    limit 8&quot; \
  | sqlite3 books.db</code></pre>
<figcaption>
figure: a sqlite query designed to showcase “.mode markdown”
</figcaption>
</figure>
<p>Gives me this table:</p>
<table>
<colgroup>
<col style="width: 42%" />
<col style="width: 25%" />
<col style="width: 11%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th>Title</th>
<th>Author</th>
<th>Format</th>
<th>Shelf</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>How to Take over the World</td>
<td>Ryan North</td>
<td>Audiobook</td>
<td>currently-reading</td>
</tr>
<tr class="even">
<td>I Must Be Dreaming</td>
<td>Roz Chast</td>
<td>Hardcover</td>
<td>read</td>
</tr>
<tr class="odd">
<td>The Other Significant Others</td>
<td>Rhaina Cohen</td>
<td>Audiobook</td>
<td>read</td>
</tr>
<tr class="even">
<td>Justice League vs. Godzilla vs. Kong</td>
<td>Brian Buccellato</td>
<td>Hardcover</td>
<td>read</td>
</tr>
<tr class="odd">
<td>Better Living Through Birding</td>
<td>Christian Cooper</td>
<td>Audiobook</td>
<td>read</td>
</tr>
<tr class="even">
<td>Braba: A Brazilian Comics Anthology</td>
<td>Rafael Gramp</td>
<td>Paperback</td>
<td>read</td>
</tr>
<tr class="odd">
<td>The Penguin Vol. 1: The Prodigal Bird</td>
<td>Tom King</td>
<td>Paperback</td>
<td>read</td>
</tr>
<tr class="even">
<td>Space-Mullet Volume 1</td>
<td>Daniel Warren Johnson</td>
<td>Paperback</td>
<td>read</td>
</tr>
</tbody>
</table>
<h2 id="conclusion">Conclusion</h2>
<p>I’m a big fan of using as little database as possible when starting a
new project. I think recutils is super great for prototyping and then
throwing away.</p>
<h2 id="resources">Resources</h2>
<ul>
<li><p><a
href="https://twtxt.readthedocs.io/en/latest/user/intro.html#intro"
class="uri">https://twtxt.readthedocs.io/en/latest/user/intro.html#intro</a>
Twtxt, a “timestamp + line of text” microblogging format</p></li>
<li><p><a href="https://www.gnu.org/software/recutils/"
class="uri">https://www.gnu.org/software/recutils/</a>
<abbr title="GNUs Not UNIX">GNU</abbr> recutils</p></li>
</ul>
</main>
<footer>
<p>
:wq
</p>
<div style="text-align: center; margin: 2rem 0;">
𐡸 𐡸 𐡸
</div>
<p><a href="mailto:christopher.p.brown@gmail.com?subject=RE: Incremental Databases">Reply
by Email</a></p>
<p>
Filed under:
</p>
<ul>
<li>
<a href="t-sqlite.html">sqlite</a>
</li>
<li>
<a href="t-recutils.html">recutils</a>
</li>
<li>
<a href="t-database.html">database</a>
</li>
</ul>
<p>
<a href="54.html">« Previous</a> | <a href="55.html">2024-09-07</a> |
<a href="56.html">Next »</a>
</p>
</footer>
</body>
</html>
