#!/bin/zsh
awk '
BEGIN {
  main=0
}
{
  if (FILENAME == "db/abbr.txt") {
    split($0, fields, ":")
    abbr[fields[1]] = fields[2]
    next
  }
  if ($0 ~ "^```") { codeblock=!codeblock }
  if ($0 ~ "^changequote") { main=!main }
  for (a in abbr) {
    if (main && !codeblock && abbr[a] != a)
      for (f = 1; f <= NF; f++) {
        s = tolower($f)
        gsub(/[[:punct:]]/, "", s)
        if (s == tolower(a) && abbr[a] != a) {
          $f = "<abbr title=\"" abbr[a] "\">" $f "</abbr>"
          abbr[a] = a
        }
      }
  }
  print $0
}' db/abbr.txt $*
