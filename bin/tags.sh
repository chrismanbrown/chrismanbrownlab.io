#!/usr/bin/env sh
find src -name '[0-9]*.m4' -exec grep -o "__id, [^)]*\|__keywords, [^)]*\|__title, [^)]*" {} \; \
  | awk ' /__id/ { printf "%s\n",$0; next } { printf "%s ",$0 }' \
  | sed '/^__id/d' \
  | sed 's/^__title, `\([^'\'']*\)'\'' __keywords, `\([^'\'']*\)'\'' __id, \(.*\)$/\3|\1\t\2/' \
  | sed '/^__title/d' \
  | sed '/\t$/d' \
  | sed 's/, / /g' \
  | awk '
    BEGIN {
      FS="\t";
    }
    {
      split($2, _tags, " ")
      for (_t in _tags) {
        t = _tags[_t]
        TAGS[t] = (length(TAGS[t])==0) ? $1 : TAGS[t] "@@" $1
      }
    }
    END {
      header="include(src/header.html)"
      indextitle=sprintf("%s\n# %s\n",header,"All Tags")
      cmd="echo \"" pagetitle  "\" > src/t-" t ".m4"
      system(cmd)
      cmd="echo \"" indextitle  "\" > src/t-index.m4"
      system(cmd)
      for(t in TAGS) {
        split(TAGS[t], ids, "@@")
        pagetitle=sprintf("%s\n# %s\n",header,t)
        cmd="echo \"" pagetitle  "\" > src/t-" t ".m4"
        system(cmd)
        cmd="echo \"\n## " t " \" >> src/t-index.m4"
        system(cmd)
        for(i in ids) {
          split(ids[i],idtitle,"|")
          line=sprintf("- [%s](%s.html)",idtitle[2],idtitle[1])
          cmd="echo \"" line  "\" >> src/t-" t ".m4"
          system(cmd)
          cmd="echo \"" line  "\" >> src/t-index.m4"
          system(cmd)
        }
        line="- [All Tags](t-index.html)"
        cmd="echo \"" line  "\" >> src/t-" t ".m4"
        system(cmd)
      }
    }
  '
