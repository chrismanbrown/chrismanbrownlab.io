LATEST := `cat LATEST`

# list all recipes
default:
  just --list --unsorted

# start new post
new:
  #!/usr/bin/env sh
  latest=$(( {{LATEST}} + 1 ))
  echo $latest > LATEST
  $EDITOR src/$latest.m4

# build html and rss
build:
  touch src/list.m4 src/feed.m4 && make all

# show a glossary
glossary:
  grep -H -B 1 "^:" src/*.m4

# build assets
assets:
  rsync -vurp static/* www/

# build tags
tags:
  sh bin/tags.sh

# build wiki
wiki:
  mkdir -p www/wiki && rsync -vurp ~/knowledge_html/* www/wiki/

# watch for changes
watch:
  ls src/*.m4 | entr -r make $(cat LATEST).html

# clean build
clean:
  rm www/*.html www/rss.xml

# build all
all: assets build wiki tags
