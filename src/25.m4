define(__timestamp, 2021-11-21)dnl
define(__title, `Top Ten Things I Like About Deno')dnl
define(__subtitle, `a review of the javascript runtime')dnl
define(__keywords, `deno, node, javascript')dnl
define(__id, 25)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

## Contents

1. [Intro](#intro)
2. [Top level async/await](#top-level-asyncawait)
3. [TypeScript](#typescript)
4. [Tooling](#tooling)
5. [Dependencies](#dependencies)
6. [A Standard Library](#a-standard-library)
7. [Sandboxed By Default](#sandboxed-by-default)
8. [ES Modules](#es-modules)
9. [Documentation](#documentation)
10. [Conclusion](#conclusion)

## Intro

This weekend I spent some time writing a little command line utility in typescript using [deno](https://deno.land/), the next generation javascript runtime from the maker of node.js.

It was pleasant.

Here are my top ten favorite things about using deno.

## 1. Top level async/await

This is really nice, but it's also kind of a gotcha for me because I get used to just dropping `await`s wherever and whenever I want, but then you still can't drop them inside of a function without first making it `async`. Which I'm prone to forget to do now.

## 2. TypeScript

To be honest, I'm not very good at TypeScript. I've never had to use it extensively. But I like the idea of it, and will probably continue to try to use it more thanks do deno supporting it out of the box. No babel, webpack, etc needed.

## 3. Tooling

Deno ships as a single binary that includes a formatter (prettier), a linter, and a test runner. It's so nice to be able to just start working on a project and not have to set up and configure all your own tooling.

The tooling also includes a compiler so that you can create a binary for your own architecture or for a target. This is exciting because it makes Java/TypeScript feel more like a "real" systems language in which you can create little scripts and apps and utilities.

## 4. Dependencies

I prefer to describe things as what they are as opposed to what they are not. But in this case, node.js got dependencies / package management so wrong that there's really no better way to describe deno's approach as "not node's". There is no `node_modules` folder and no `package.json`.

Instead, you import modules directly by their URL just like you would in the browser.

Convention is to create a `deps.ts` file at your project root and fill it with a bunch of exports:

```javascript
export { module } from "https://unpkg.com/liltest@0.0.5/dist/liltest.js";
```

You'll notice that you can lock in a specific version as part of the URL.

Dependencies are downloaded once and then cached until you reload.

You can view your entire dependency tree with `deno info mod.ts`.

## 5. A Standard Library

Deno has [a standard library](https://deno.land/std@0.115.1) that puts node.js to shame.

It also has [an extensive repo of third party modules](https://deno.land/x).

## 6. Sandboxed By Default

Deno is a secure runtime that is sandboxed by default. It cannot read or write or access the net. You must intentionally allow these privileges, and you can do so in an incremental way: you can allow read/write access only to specified files.

## 7. ES Modules

A minor feature, but it's just nice not having to contend with node's CommonJS modules.

Consistency is nice. One less thing to worry about. You can just *start working*.

## 8. Documentation

The [deno.land docs browser](https://doc.deno.land/) is pretty good. Documentation for exports is generated from jsdoc style comments using `deno doc`. (See "Tooling" above.)

You can also type check examples in your comments with `deno test --doc mod.ts` to make sure your examples are always up to date.

The [manual](https://deno.land/manual) also feels very complete.

## Conclusion

That's it. I guess there are only 8 things that I really like about deno after all. But I like them a lot. I've said it a couple times, but the major overall feeling is that deno allows you to *just start* on a project and not have to configure your project and your transpiler and webpack and wrestle with your dependencies.

It feels like a much more mature and *useful* platform than node.


changequote`'dnl change quotes `back to default'
include(src/footer.html)
