define(__timestamp, 2024-03-08)dnl
define(__title, `doing a blog')dnl
define(__subtitle, `why you even gotta do a thing on the internet')dnl
define(__keywords, `potpourri')dnl
define(__id, 46)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

## CONTENTS

1. [INTRODUCTION](#introduction)
2. [WHERE DO BLOGS EVEN COME FROM](#where-do-blogs-even-come-from)
3. [WHERE DID BLOGS GO](#where-did-blogs-go)
4. [WHY DO A BLOG](#why-do-a-blog)
5. [HOW I BLOG](#how-i-blog)
6. [COMING UP WITH IDEAS](#coming-up-with-ideas)
7. [TURNING IDEAS INTO BLOG POSTS](#turning-ideas-into-blog-posts)
8. [CONCLUSION](#conclusion)

## INTRODUCTION

I recently started writing stuff and posting it on the web.

This is called "Doing A Blog" (DAB).

You may be wondering where blogs come from and why you would do a blog.
If so,
keep reading!

## WHERE DO BLOGS EVEN COME FROM 

In the early 1990s 
some guy named Tim
invented the World Wide Web
(very successful!)
and wrote what is considered to be the very first blog.
Later in the 1990s,
I was writing HTML, CSS, and JavaScript
in Notepad.exe
and uploading it to Geocities
via FTP.

So you could say that Tim and I
really kicked this whole thing off.

## WHERE DID BLOGS GO

During the Golden Age of Blogging,
all of the blogs used to be united
in discourse
as part of the blogosphere
but then Google killed RSS
and all the bloggers moved to Facebook.
And now the industry would have you believe
that now you have to use frameworks
and libraries
and transpilers
and bundlers
just to put words on a page.
But you mustn't listen to their sugary lies, child.

## WHY DO A BLOG

Why do a blog?

There are lots of reasons to <abbr title="Do A Blog">DAB</a>.

Mine include:

1. REFLECTION AND JOY:
   Writing is thinking.
   It helps me investigate and explore my beliefs,
   and to examine them and articulate them.
   Helps me get organized about the things I think are true.
   This has always been true for me.
   The act of thinking and writing brings joy.

2. SHARING:
   Writing for myself is satisfying enough,
   because of my vanity and ego.
   But I also like writing for others!
   There are a lot of posts here
   that are written to very specific people or peoples.
   I don't know if you can,
   but I can tell a big difference in tone of voice
   in the pages on this blog
   depending on who I'm writing to.
   Sometimes it's to a friend to share something cool with them
   or to answer a question about something.
   Sometimes it's to a coworker to explain something in detail
   so I can share it with them later.

3. GROWTH:
   I build things for a living.
   Building a blog/website is an opporunity to create something on my own terms,
   in the way I want to.
   To try out new technologies and embrace or reject them.
   It's a fun sandbox and test environment.

## HOW I BLOG

I have lots of blogs.
I spin one up for new interests and niche topics all the time
because blogs are cheap.
In fact Github / Gitlab pages,
neocities,
and other static hosts
are all free!
All it costs is you is a little time and effort.

This blog and a couple others
still use m4-blog,
which I don't really recommend any more
or even especially enjoy using at this point.
It's perfectly functional.
But who wants to actually wrestle with m4?

https://github.com/chrisman/m4-blog

Some more recent recent blogs
just use pandoc:

https://git.tilde.town/dozens/pandoc-blog

Still others are just huge blobs of text
in a single file
that get transformed into a huge blog of html:

https://git.tilde.town/dozens/gamelog

## COMING UP WITH IDEAS

A lot of my blogs are really tightly focussed on things like
"Here are some notes on every game I play"
or "Here's a review of every book I read."
For those ones,
I don't have to come up with ideas for blog posts.
I just have to follow up
after completing a task
and write one.

Other blogs,
like this one,
are less lazer focused.
The theme of this blog is "tech blog"
so I write about technology
and programming
and web dev
and stuff.

This is harder,
because I have to come up with ideas
and then convince myself that they are "good."

The easiest thing for me to do
is to take something me and my friends are talking about
and are excited about,
and then write about that.
For example,
we all just agreed to write
"Blogging About Blogging"
posts,
and that's why this post exists.

I have an "ideas.txt" file on my computer
that I will slam ideas into as I have them.
(Most of them don't turn into anything.)
And almost all of my blogs also have a "drafts" folder
where I can jot notes
and develop ideas before turning them into posts.
This post, for example, is actually
developed from a draft
originally created in 2020!

## TURNING IDEAS INTO BLOG POSTS

In terms of developing an idea into a post,
I tend to think fairly heirarchically.
I like to organize thoughts into sentences and paragraphs,
and then into sections
that fit nicely under an `<h2>`.

Some call this "outlining"
and I tend to do it as I go along.

This process is often
very organic and playful.
I end up discovering the underlying structure 
of my thoughts
(and consequently my writing)
as I go.
This leads to rearranging and reorganizing
a lot of stuff,
and deleting a lot of stuff.

## CONCLUSION

Okay this has been a "Blogging About Blogging" post!
The main thing to know about blogging
is that it never has to be good or complete.
You just gotta decide to do it.

Here's a little framework for you in closing:

1. Identify your ROLE: learner, or expert
2. Identify your VOICE: descriptive, or instructive


|         | Descriptive                  | Instructive           |
|---------|------------------------------|-----------------------|
| Learner | Here's something I learned.. | There were challenges.|
| Expert  | Here's how this works..      | Use this solution..   |

The difference between Learner and Expert should already be apparent to you. "Descriptive" means you're just describing a process or a learning experience. Whereas "Instructive" means that you are giving instruction on how to do something. Sharing opinions about the right vs. wrong way to do things. "Descriptive vs Instructive" can also be thought of "Theoretical vs Practical."

This might help you focus your perspective and your goals for writing.
And a lot of the time,
it might free you from the pressure
of being a total expert in what you're writing about.
It's totally okay to just write about something cool you learned
or made while you were messing around with something new.

Now get out there and Do A Blog!

changequote`'dnl change quotes `back to default'
include(src/footer.html)
