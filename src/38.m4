define(__timestamp, 2024-01-11)dnl
define(__title, `Groff I')dnl
define(__subtitle, `whence and wherefore')dnl
define(__keywords, `groff')dnl
define(__id, 38)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

This is part of the Groff Series:

- Groff I: Whence and Wherefore
- [Groff II: Requests, Escape Sequences, and Macros](42.html)

## CONTENTS

1. [INTRODUCTION](#introduction)
2. [WHAT IT IS](#what-it-is)
3. [HISTORY](#history)
4. [WHY](#why)
5. [CONCLUSION](#conclusion)

## INTRODUCTION

I learned a lot about groff in 2023,
and used it for a lot of personal projects and documents.
I am writing this to share with you what I learned!

This will be an introduction to what groff is,
where it came from,
and why I enjoy using it.
Future posts in this series will go over how to get started actually using it.

## WHAT IT IS

groff is plain text markup for typesetting and document formatting.

The markup looks like this:

```groff
.TL How To Steal Horses and Rustle Cattle
.AU Billy the Kid
.SH
On Being An Outlaw
.PP
groff is a plain text markup....
```

And then it outputs beautiful PDFs!

## HISTORY

In the 1960s there was a program called `RUNOFF`.
As in to "run off" a job to the printer.
This eventually became `roff`, and then `troff` for UNIX,
which was used to format man pages. 
When the Bell Labs patent department started using `troff` for word processing,
it became the first UNIX program to have an external client.
This established UNIX as a credible operating system
and secured funding for the project.

In 1990 GNU released a free version of roff called GNU roff, or `groff`, bringing roff to the masses.

But the masses eventually decided they could do better.

In the late 1970s, Donald Knuth created `TeX` to typeset the second edition of his book
*The Art of Computer Programming*
because he hated the way it looked after the printing industry
transitioned from hot metal typesetting to phototypesetting.
The popular `LaTeX` macro package for `TeX`
seems to have become the standard for typesetting markup in academia
and has largely obsoleted `groff`.

Since around 2011, groff is not even used to create man pages anymore,
which was like, it's whole deal.
BSD, MacOS, and some linux distributions including Alpine
now use `mandoc` instead.

And then I came along. In 2023, I read a few posts on <https://text.causal.agency/> and really enjoyed the novelty and the aesthetic of using man pages as a blog post format.
I tried writing a few documents in `mdoc` and then `groff`,
and then I just continued with the deep dive into groff over the next couple of months.
I wrote some letters and memos,
and published a few essays and even a few tabletop roleplaying games using groff,
and I used it to format a plain text newsletter.

## WHY

So what's the deal?
Why bother with this obsolete, obtuse markup language in year 2024?

- It is small and it runs on machines with limited resources. In the Alpine package repository, the core TeX Live distribution is 500mb installed.
`groff` is 8mb.

- It is widely available. Because of its history with man pages, groff is available on most linux and POSIX compliant systems.

- It is scriptable and embeddable. Here's an example of how `tkts` uses a small groff template to create a pdf invoice:
<https://gitlab.com/Durrendal/tkts/-/blob/master/src/tkts.fnl?ref_type=heads#L22>

- It is "medium sized." If I need little to no control of the typesetting or layout of a document, I'm most likely to pipe some markdown through pandoc to create a pdf. If I need an extraordinary amount of typesetting and layout control, I will probably use a word processor or even a desktop publisher like Scribus. If I need a medium to large amount of control, I use groff.

- It is fun. I personally find a great deal of satisfaction in using old, weird software in new and surprising ways.

- It is a suite of tools. groff comes with preprocessors for creating tables (`tbl`), pictures and diagrams (`pic`, `grn`), charts and graphs (`grap`), and bibliographies (`refer`). I have found `tbl` specifically to be of great use even outside of groff. I sometimes use it to create markdown tables.

## CONCLUSION

That's it for this introduction to groff.
In the next post in this series,
I'll go over how to get started with groff's macro packages.

changequote`'dnl change quotes `back to default'
include(src/footer.html)


