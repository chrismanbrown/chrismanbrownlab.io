define(__timestamp, 2023-11-07)dnl
define(__title, `i got locked out of building my static website')dnl
define(__subtitle, `the node.js ecosystem is a shifting pile of quicksand')dnl
define(__keywords, `node, vue, nuxt')dnl
define(__id, 37)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

Most of us have a friend of a friend who it's happened to.
And this time it happened to me.

I have a small static website
that I haven't looked at in a couple of months.
It's a trivial site.
A blog type thing full of recipes and reviews. 
This week I went to go make a small update to it,
and found I wasn't able to build it.

I did it to myself, really.
I knew this might happen eventually.
I even expected it to some extent.
But it's still annoying and irksome now that we're here.

Usually when I make a new personal bloglike site,
I just make a pile of markdown files and pipe them through some transforms to make the final HTML.
This time though,
I wanted to try some ✨ TECHNOLOGY ✨.
I had just used vue.js in a project at work,
and I liked it.
So I decided to use Nuxt.js,
a vue framework,
and the community's answer to React's Next.js.
I set up Nuxt's 'content' module
so I could use Nuxt's cool directory based routing
with plain markdown files.
And I also set it up to use tailwind.css
and pug templates
so I never had to sully my hands by writing dirty HTML tags at all.
It was all just nested chains of utility classes as far as the eye could see.

<figure>
<img src="img/pugwind.jpg" alt="a code snippet shopped to look like a crappy phone photograph of a computer monitor showing a text editor full of tailwind.css">
<figcaption>
Look at this hot mess. Just look at it. (Code snippet courtesy of <https://code.xxut.ru/>)
</figcaption>
</figure>

Was it pretty?
No.
Did I enjoy writing it?
Not especially.
I'd hate to have to maintain it honestly.
But I only really had to write it once,
and then I got all of these great tailwind colors and animations and stuff.
And then I could just write my recipes for vegan waffles or whatever and never have to look at it again.

Until it happened.

Like I said, it had been a few months since I touched my vegan waffles blog.
Poor neglected thing.
But then I went in this week to make a small update.
Literally to just add a few links to the footer.
And the thing wouldn't build.

No worries,
I thought to myself.
I've encountered this kind of thing a million times.
I'll just delete `node_modules`
and reinstall.
Which I still think should have worked; I had a lockfile.
But this introduced more broken dependencies.
Shoot, did I use the wrong package manager?
I tried again with yarn.
And again with npm just for the heck of it.
Am I on the wrong version of node/npm?
I don't know, I don't have an `.nvmrc`.
Heck, I'll just upgrade all the dependencies.

I could eventually build.
But got a ton of runtime errors.
But hey, I could build.
So I read through the Nuxt3 migration guide,
and the new 'content for Nuxt3' migration guide,
and tried to smooth out all the bugs for a little over an hour.
I think. I don't know, I didn't start a pomodoro or anything.

Eventually I got frustrated enough that I just checked out a new branch,
ripped out everything except for the folder of markdown,
and wrote a quick pandoc pipeline to spit out some static HTML.
And I vowed to never use a huge framework or library again
for a simple static bloglike.

So now I'm missing all my cool tailwind utility classes.
I might slowly add some colors and animations back by hand,
because I'm kind of fond of the way it looked.
But for now I'm happy that the content is still there.
Including a couple of new links in the footer.

Lessons learned?
I don't know, I could have committed an `.nvmrc`.
That might have helped?
Containers?
That might have helped, at the cost of overengineering my little static site even more.
I think the real lessons are
1) always keep your content separate---it was great to be able to just plug it into a new build pipeline, and
2) experiment with shiny new tech if you want to,
but don't rely on it unless you're willing to commit to it---I didn't keep up with vue or Nuxt, and didn't bother learning it super well to begin with, so I was super unprepared for the big migration and the breaking changes.

This site itself breaks lesson number 1:
all of my content is embedded in m4 files.
It's regluar though.
I could write a few s/ed scripts to liberate it
and save it as markdown with yaml frontmatter.
Which is kind of how I like all of my content to be.
(Unless it's in a recfile, but that's another matter.)
And the only way in which it violates lesson number 2
is that I still do not grok how GNU Make actually works.
I had to fight with it just now in fact to get it working again
after moving a few directories around.

Maybe a pandoc+markdown / ninja build rewrite of this site is in order.

changequote`'dnl change quotes `back to default'
include(src/footer.html)
