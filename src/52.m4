define(__timestamp, 2024-08-06)dnl
define(__title, `how to irc over xmpp')dnl
define(__subtitle, `chats within chats')dnl
define(__keywords, `guide, irc, xmpp, biboumi')dnl
define(__id, 52)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

The intended audience
of this article
is my friend Will,
or anybody else who is already familiar
with IRC and XMPP,
and wants to know how to
connect to IRC over XMPP.

## Preliminaries

Nonetheless,
some preliminaries.
In case
you are not the intended audience.
(I commend your curiosity!)

IRC is an internet chat protocol
that has existed more or less
since the beginning of the internet.
To learn more about it,
check out the following resources:

- https://libera.chat/guides/basics
- https://modern.ircdocs.horse/

A few irc servers to join and chat on are:

- https://tilde.chat/
- https://libera.chat/

XMPP is another, more different internet chat protocol
that has existed not quite as long as IRC.
It is quite a bit more complex than IRC.
It is defined by over a dozen RFCs
and has probably over a hundred expansion packages.
But that's all on the backend!
In practice,
xmpp is pretty straightforward and easy to use!

https://xmpp.org/getting-started/

Biboumi is a bridge that allows you
to connect to IRC servers
over XMPP.

https://doc.biboumi.louiz.org/

There are a couple of biboumi services that I know of:

- https://tilde.team/wiki/xmpp#biboumi
- https://hmm.st/#biboumi

Okay that's enough of the preliminaries.

## Profanity

Once you have a xmpp account and a client,
you'll want to start ircxmmp-ing!

In order to do this,
you will need to use a client
that can configure ad-hoc commands.

I had never heard of ad-hoc commands before.
And the only client I found that allowed configuration of them
is Profanity.

https://profanity-im.github.io/index.html

Profanity is a text-based xmpp client.
It's written in c and uses ncurses,
and claims to be the Irssi of xmpp clients.
(Irssi is a venerable IRC client.)

Also,
Profanity is already installed on the pubnix server
where I hang out.
Convenient!

## Ad-Hoc Commands

Okay,
fire up your Profanity.
Check out `/help` and `/help account`
and then log into to your xmpp server.

Now,
you need to build the id of the server you want to connect to.
It will look like `<irc-server>@<biboumi-server>`.
e.g. `irc.libera.chat@biboumi.sports.horse`.
Open up a chat window in Profanity with
`/msg irc.libera.chat@biboumi.sports.horse`.

Now you're ready to execute some ad-hoc commands
and configure some options!
In the chat window,
type `/help cmd`.
And then `/cmd list`.
And then `/cmd exec configure`.

This will drop you into the config editor,
which is a "form."
Read `/form help`.
And look at `/form help field1` etc.
And notice that to set a field,
you just do e.g. `/field1 <value>`.

The fields are also described in the biboumi docs.

https://doc.biboumi.louiz.org/9.0/user.html#id2

## Tragedy

The real tragic part
about this post
is that I did this configuration once
a really long time ago
and never had to revisit those settings ever again.
So I do not remember which fields to set,
nor what to set them to.
You're on your own for this part,
I fear.
You're smart,
and you have the tools you need
to be successful.
You got this.

## Finally

Once the server is configured,
you can now join a channel
on that irc server
on the xmpp client of your choice
by joining a MUC
with the JID `#<irc-channel>%<irc-server>@<biboumi-server>`.

That's it.
That's all I got.
So long
and see you in the funny papers.

changequote`'dnl change quotes `back to default'
include(src/footer.html)
