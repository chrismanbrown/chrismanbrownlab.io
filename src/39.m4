define(__timestamp, 2024-01-29)dnl
define(__title, `Re: Making a single-file executable with node and esbuild')dnl
define(__subtitle, `or Make a singe-file executable with deno')dnl
define(__keywords, `javascript, deno')dnl
define(__id, 39)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4


## INTRODUCTION

This is a response to "Making a single-file executable with node and esbuild" by Bill Mill of billmill.org,
which is a fantastic write-up of the somewhat complicated steps required to make a single-file executable with node.js and esbuild.

I want to begin by hedging my response and acknowledging the disingenuousness of offering "just use deno"
when A) using deno, and potentially B) "most simple and least number of steps" are probably not the point of Bill's post.
The point is most likely to document the steps for making a single-file executable with node.js and esbuild.
Which he more than accomplishes.

This is offered in case somebody else out there also wants to make a javascript executable,
and wants to consider an alternative to the process described in Bill's post.

## MAKE A SINGE-FILE EXECUTABLE WITH DENO

Let us also begin with a simple two file program that uses a dependency.

sum.js

```javascript
export function sum(ns) { return ns.reduce((x,y) => x+y, 0) }
```

index.js

```javascript
import { parseArgs } from "https://deno.land/std@0.213.0/cli/parse_args.ts";
import { sum } from "./sum.js";

console.dir(sum(parseArgs(Deno.args)._));
```

And it works thusly:

```bash
❯ deno run index.js 1 2 3 4 5 6
21
```

Slightly off topic, but notice there is no `npm init` or `npm install` steps. Deno is designed to not use npm, or node_modules.

Now, let us commence with the compiling part:

```bash
❯ deno compile --output sum index.js
Compile file:///Users/cb/sandbox/denoexec/index.js to sum
```

And it works thusly:

```bash
❯ ./sum 1 2 3 4 5 6
21
```

The end!

## RESOURCES

- https://notes.billmill.org/programming/javascript/Making_a_single-file_executable_with_node_and_esbuild.html

- https://chrismanbrown.gitlab.io/25.html Top Ten Things I Like About Deno

- https://docs.deno.com/runtime/manual/tools/compiler

changequote`'dnl change quotes `back to default'
include(src/footer.html)



