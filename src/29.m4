define(__timestamp, 2022-01-16)dnl
define(__title, `Review: Freewrite Traveler')dnl
define(__subtitle, `First Impressions')dnl
define(__keywords, `review, gadgets')dnl
define(__id, 29)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

include(src/29.md)

changequote`'dnl change quotes `back to default'
include(src/footer.html)
