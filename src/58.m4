define(__timestamp, 2024-10-21)dnl
define(__title, `zathura is a good document viewer')dnl
define(__subtitle, `i recommend it for viewing documents')dnl
define(__keywords, `recommendation')dnl
define(__id, 58)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

https://pwmt.org/projects/zathura/

I started using zathura on my macbook
because I was doing a lot of writing
where my workflow included editing source (groff) files
to create pdfs,
and I wanted a pdf viewer
that would auto-update on write
so I could easily preview my changes
without having to change window focus
or reload the document.
(zathura does this,
and it's great.)
And I liked it so much
now I use it everywhere I can install it.

It is plugin-based software.
So you need to install a pdf plugin to actually use it.
But that means it can support any other filetype
for which there is a plugin.
For example,
I use a `.cbz` plugin on my handheld device
to read comic books.

It is great for reading pdfs and comics
because it has lots of keyboard shortcuts
for quickly navigating
and resizing pages.

One of its killer features,
and I don't know why every pdf viewer doesn't do this,
is inverting the document colors with a keystroke
so that plain black text on a white background
becomes plain white text on a black background.
Now I can flip back and forth between a dark terminal
and a pdf in low light
without blinding myself,
or read pdfs comfortably in bed in the dark.


changequote`'dnl change quotes `back to default'
include(src/footer.html)

