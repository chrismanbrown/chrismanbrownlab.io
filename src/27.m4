define(__timestamp, 2021-12-30)dnl
define(__title, `Books I Read in 2021')dnl
define(__subtitle, `a year in review')dnl
define(__keywords, `review, books')dnl
define(__id, 27)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

## Contents

0. [Introduction](#introduction)
0. [Nonfiction](#nonfiction)
0. [Graphic Novel](#graphic-novel)
0. [Fiction](#fiction)
0. [Experimental](#experimental)
0. [Cutting Room Floor](#cutting-room-floor)
0. [Resources](#resources)


## Introduction

Every year I do a retrospective on the books I read:

- [2020](21.html)
- [2019](2.html)

This year I read 23 books, which is a very low number for me. I usually read around 80.

I continue to read significantly less while working from home because I used to devour audiobooks during my commute. I also didn't really read many graphic novels, or comic books or game books this year, which also usually boosts my number a lot. Graphic novels/comic books because the libraries were still closed for a lot of the year. (And I prefer my sequential art in print.) And game books because I've been focused this year on small self-published games from itch.io instead of big games from major publishers.

## Nonfiction

I tried to arrange these from best to worst.

1. [The Great Influenza](https://www.goodreads.com/book/show/29036.The_Great_Influenza) - Best book of 2021. A fascinating account of the 1920 influenza. Especially poignant given, you know. Fun fact: the "Spanish Flu" most likely originated in Kansas. Spanish newspapers were simply the first to extensively report on it. And we've always been very good at othering and assigning blame elsewhere.

2. [Nomadland](https://www.goodreads.com/book/show/41437721-nomadland) - Literally don't shop at Amazon anymore. I'm sure fulfillment centers are also bad at other companies too. But seriously, Amazon's disregard for the well-being and welfare of its warehouse employees is unforgivable at this point. Itinerant, migrant workers in the US might seem like something that phased out with the dust bowl, but it's still very much a thing. I struggle with my own privilege while reading this book and can't escape the thought that van life might be fun without all the poverty and harassment and danger and stuff.

3. [The Library Book](https://www.goodreads.com/book/show/39507318-the-library-book) - So good! About the Los Angeles public library fire, and sprawls out into libraries and arson in general.

4. [Too Much and Never Enough](https://www.goodreads.com/book/show/54081499-too-much-and-never-enough) - Trump family biography. Ain't nobody in that family is okay. One of the first books I read this year, and the first current events/political book I've read in over a year thanks to the complete and utter burnout I suffered from Trump + Covid.

5. [How To Change Your Mind](https://www.goodreads.com/book/show/40626244-how-to-change-your-mind) - History (and future?) of natural and synthetic psychedelics (mushrooms and LSD)

6. [Why's Poignant Guide To Ruby](https://www.goodreads.com/book/show/463882.Why_s_Poignant_Guide_to_Ruby) - Nostalgia read. _why is one of the reasons I started progamming in earnest, and is the forefather of all today's quirky code books like *Learn You A Haskell For Great Good* and *Clojure for the Brave and True*. I had forgotten how openly he incorporated his struggles with mental health into the ending.

7. [On The House](https://www.goodreads.com/book/show/57610828-on-the-house) - John Boehner's memoirs. I didn't know much about his background before this.

8. [Helgoland](https://www.goodreads.com/book/show/56307401-helgoland) - Something about the invention of quantum physics. It didn't really stick.

## Graphic Novel

- [Bernie](https://www.goodreads.com/book/show/25938003-bernie) - What it says on the tin: a biography of Bernie Sanders. It was fine.

- [I Am Not Okay With This](https://www.goodreads.com/book/show/34445185-i-am-not-okay-with-this) - A young adult novel that glorifies suicide. Utter trash. Doesn't deserve to be in print.

## Fiction

I tried to arrange these from best to worst.

1. [The Starless Sea](https://www.goodreads.com/book/show/48710099-the-starless-sea) - Best of the year. So dreamy and evocative. I read some criticism about how some of the characters develop, and in one sense I think that's valid, but in another sense, it's kind of irrelevant because of how everything and everyone in the story is in service of the meta-story about where do stories come from and how does imagination work. There are other books on this list that are probably just as good, but none of them give me the magic feels that this one did.

6. [The Glass Hotel](https://www.goodreads.com/book/show/51842737-the-glass-hotel) - Hauntingly beautiful. I still think about "invisible worlds" a la how the one guy in shipping sees ships and routes when looking at everyday objects.

2. [In An Instant](https://www.goodreads.com/book/show/51037979-in-an-instant) - Self preservation and motivations develop in unexpected ways when you're all about to freeze to death on the side of the road, and in the aftermath relationships and changed and/or destroyed

5. [The City We Became](https://www.goodreads.com/book/show/43558961-the-city-we-became) - At this point I'll read anything by Jemisin. This was a fun urban fantasy adventure. I think I would like it more if I knew more about NYC or had ever been there.

3. [Stardust](https://www.goodreads.com/book/show/5577844-stardust) - Cute modern fairy tale. Very well done.

7. [Such A Fun Age](https://www.goodreads.com/book/show/43923951-such-a-fun-age) - Racism is subtle and complicated

4. [Magic For Liars](https://www.goodreads.com/book/show/49679109-magic-for-liars) - A murder mystery at Hogwarts but for adults. (I almost guarantee you this started as HP fanfic.)

8. [Ninth House](https://www.goodreads.com/book/show/43263680-ninth-house) - Paranormal ghost hunting in Yale secret societies. Good fun.

9. [Middlegame](https://www.goodreads.com/book/show/45692230-middlegame) - Strange one. Good if you want a paranormal thriller about psychic twins and a childrens book full of prophesy and a "Math plus Language" conceit. (Which, for me, distractingly made me think of the Phantom Tollbooth the whole time.)

10. [Warbreaker](https://www.goodreads.com/book/show/7438039-warbreaker) - Basic plot and intrigue.. it feels it was basically written as an excuse to explore an idea Sanderson had for a magic system. The whole rest of the story feels second to that element.

11. [Horrorstor](https://www.goodreads.com/book/show/13129925-horrorst-r) - cheesy horror: what is IKEA was haunted house

12. [The Dreamthief's Daughter](https://www.goodreads.com/book/show/7629332-the-dreamthief-s-daughter) - tropey fantasy with talking animals but also half takes place in nazi germany? All over the place.

## Experimental

- [In The Dream House](https://www.goodreads.com/book/show/49670670-in-the-dream-house) - in a class by itself is this memoir of an abusive relationship. Each chapter is like a piece of flash fiction in which the relationship itself wears a new mask and adopts a new persona.

## Cutting Room Floor

Stuff I didn't finish, and other things.

- [Love in the Time of Cholera](https://www.goodreads.com/book/show/22678050-love-in-the-time-of-cholera) - My only tangible book of the year that isn't a graphic novel. Plucked it up on a whim from a Little Free Library on a whim because I adore everything else I've read by GGM. It's beautiful. Just need to get back into it. I would read the opening vignette about the man and his parrot over and over again.

- [Flaming Carrot Omnibus](https://www.goodreads.com/book/show/44601778-flaming-carrot-omnibus-volume-1) - Surreal absurdist dadaist crimefighter whose head is a carrot that is on fire and who wears flippers on his feet. Recommended by ~vilmibm. Very funny, very silly.

- [Brand New Cherry Flavor](https://www.goodreads.com/book/show/11445769-brand-new-cherry-flavor) (Currently Reading.. Slowly) - biggest love/hate relationship I've had this year. Was enthralled by the show on Netflix, and wanted to check out the book. To my surprise, this is the rare instance that the show is much better than the book. The show fixes a lot plot elements and characterization to make it *much* better. Also, the show's content ended about 20% into the book? So I just keep going to find out what happens.

- [The Devil and the Dark Water](https://www.goodreads.com/book/show/55517584-the-devil-and-the-dark-water) - a closed room murder mystery on a boat! The Holmes/Watson dynamic is interesting because the Holmes is locked up for some crime, and the Watson is his mercenary bodyguard who gets to consult with him every once in a while. It was fun, but my loan expired before I was done, so I didn't get to finish.

- [The Vanishing Half](https://www.goodreads.com/book/show/52195151-the-vanishing-half) - Bookclub book I didn't finish in time. About a town of freed US slaves that over generations try to become as light-skinned as possible. And when a prodigal child returns with two dark-skinned children of her own, stuff happens.

- [On Earth We're Briefly Gorgeous](https://www.goodreads.com/book/show/41880609-on-earth-we-re-briefly-gorgeous) - Another bookclub book I didn't finish. So beautiful. So incredibly sad.
 
## Resources

- [my goodreads](https://www.goodreads.com/user/show/409485-chrisman)
- [my goodreads 2021 year in review](https://www.goodreads.com/user/year_in_books/2021/409485)

changequote`'dnl change quotes `back to default'
include(src/footer.html)
