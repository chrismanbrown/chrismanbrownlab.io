define(__timestamp, 2021-12-01)dnl
define(__title, `Pickles')dnl
define(__subtitle, `and how they come to be pickled')dnl
define(__keywords, `brief, potpourri')dnl
define(__id, 26)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

I once worked at an agency that would embed its engineers in offsite client teams for staff augmentation. The big idea was to send in our high-quality, talented engineers to improve their velocity, processes, and culture.

I also knew a chef who once put a cucumber into a jar of pickling juices thinking that the pickling juices would somehow become more cucumbery, and then acted surprised that the cucumber instead became more pickled.

changequote`'dnl change quotes `back to default'
include(src/footer.html)
