define(__timestamp, 2022-01-21)dnl
define(__title, `vimwiki')dnl
define(__subtitle, `how and why i use vimwiki for keeping notes and tracking tasks')dnl
define(__keywords, `vim, note-taking')dnl
define(__id, 30)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

include(src/30.md)

changequote`'dnl change quotes `back to default'
include(src/footer.html)
