define(__timestamp, 2024-12-04)dnl
define(__title, `ten years of colemak')dnl
define(__subtitle, `the keyboard layout that saved my wrists')dnl
define(__keywords, `recommendation, keyboard, potpourri')dnl
define(__id, 59)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4


I think it was
sometime around 2013
I quit using the US standard Qwerty keyboard layout
and started using Colemak.
So now that it has been
10 years
more or less,
this article is about why I decided to do that,
and how I did it.

## Why

My wrists hurt real bad.
A constant kind of almost burning ache.
Typing made it worse.
Much worse.
I often wore braces to immobilize my wrists,
which helped some.
I also eventually get a split keyboard,
which helped a lot.
But nothing helped quite as much as switching away from 
qwerty to something more ergonomic.

I had read plenty questionable articles
about how Qwerty was designed to slow typists down
by moving the keys farther apart.
This isn't really true.
They layout was designed to prevent jammed keys.

I decided that I wanted to try a different keyboard layout.
The only alternative layout I had really heard of was Dvorak.
I was a little scared of it because it is so alien to Qwerty.
But then I learned about Colemak,
and it looked a lot more appealing
because a lot of is very similar to Qwerty.
Namely the 'zxcv' run, the "copy/paste" run,
is the same.
And it seem to minimize finger travel
even more than Dvorak does.

Also,
it is available nearly everywhere.
iOS, Android,
MacOS, and Linux
all provide it out of the box
an English keyboard variant layout.
So that's comforting.

At the time,
I was using Windows.
Which is the only major OS
not to natively support colemak.
No worries though,
there is a great AutoHotKey
program for it
that provides the keymapping,
and also shows a little image of the layout
if need be.
I saved the program to a shared drive,
because I had a job where I floated
from workstation to workstation.
And I warned my coworkers how to disable it
if I ever wandered off with the program still running,
which happened every once in a while.

Now I have almost no wrist pain.
Sometimes I have to get the old wrist brace out
once every 12 - 18 months
if I have been doing extended amounts of typing
on the laptop.
But colemak,
and especially colemak on a split keyboard,
has pretty much fixed
my repetitive stress injury.
Now if I ever have to switch back to Qwerty for some reason,
I feel like my fingers are flying all over the place!

## How

My strategy was to
practice for a little bit
just enough to get familiar with the basics.
And then to switch over cold turkey.
And that's what I did!

It was very challenging
to go from typing 80 - 90 wpm
to typing ~20 wpm.
I was working in tech literacy at the time
and it was a huge level up
in humility and empathy for my patrons
who had no experience with computers at all
and didn't know how to type.

For years afterward,
my typing tended to top out at about 60 wpm
and I was fine with that.
I don't really need to type any faster than that really.
And for a long time,
I wondered whether my pain got better
just because I slowed down.
At the time of this writing,
typingtest.com says my speed is 77 words per minute.
Plenty fast enough as far as I'm concerned.

The first tool I used was keyzen:

https://wwwtyro.github.io/keyzen/

This does 'kk ff kk ffff kkkk' type drills
just to teach you where the keys are.

After doing that each night
for an hour or so
for a couple days,
I switched to type-fu.

https://type-fu.com/app

type-fu gives you full words and sentences to type.
You'll have to go into settings
to set the layout to colemak
and to disable sounds.

Finally,
I used amphetype
which allows you to provide your own text
to type.

https://pypi.org/project/amphetype/

I downloaded a book from project gutenberg
and typed it out.

It was about halfway through this step
that I quit qwerty entirely.

I let all my coworkers know that I was going to be
very slow to respond to email for the next couple weeks.

I had to refer to a visual reference
quite a lot
throughout each of these steps.
When I wasn't using AutoHotKey's
little heads up display,
I kept an image of the layout
as my computer desktop wallpaper
so I could quickly glance at it
with a gesture.

## Mobile

For a while,
I was typing colemak on desktop
and qwerty on mobile.

But once I really realized that qwerty
wasn't necessary,
preferable,
or efficient
most of the time,
I really got the alt-keyboard bug.
I wanted to use a keyboard
specifically designed for mobile.

I used MessagEase for a while.

https://en.wikipedia.org/wiki/MessagEase

But then I decided to unify my typing experience
and just use colemak everywhere.

Initially I was skeptical
of the typing experience
using colemak with a swiping keyboard.
Colemak is so home-row oriented,
won't swiping just be a bunch of linear left and right?
Turns out,
in practice,
yes it is!
And it's not a problem!
Swiping keyboards are good at what they do.

## Now

Like I said earlier,
I went from terrible wrist pain
to almost none at all.

The downside
is that I have lost almost all of my Qwerty
typing ability.
This is less of an issue than you might fear
because as I said earlier,
you can easily pull it up almost anywhere.

Currently
I use google's gboard on iOS,
and macos's native support.

I have successfully convinced one other person
that I know of
to try colemak.
They started using it shortly
after I started,
and are also still using it today.

## Resources

- https://colemak.com/
- https://first20hours.com/typing/

changequote`'dnl change quotes `back to default'
include(src/footer.html)
