define(__timestamp, 2024-04-15)dnl
define(__title, `frontmatter.rec')dnl
define(__subtitle, `a consistent metadata schema')dnl
define(__keywords, `recutils, markdown, writing, metadata')dnl
define(__id, 50)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

I write a lot of documents.
Blog posts, articles, essays, games, slides, etc.
And the documents I write target a lot of different formats:
html, epub, plain text, pdf.
For almost every document I write,
there is an amount of metadata
that I am interested in maintaining.
That metadata usually manifests as "frontmatter"
because I usually draft everything I write
in markdown before potentially converting it
to some other format
(such as groff)
for presentation.

And so I present to you
`frontmatter.rec`,
the document metadata
I consider mandatory
for pretty much everything I write.

<figure>

```rec
%rec: frontmatter
%doc: minimal acceptable document metadata
%mandatory: title type description
%allowed: title type description
%type: title,description line
%type: type enum string datetime list
%key: title

title: title
type: string
description: the title of the document

title: description
type: string
description: a summary, description, or tagline of the document

title: author
type: string
description: the author of the document

title: version
type: string
description: document version

title: created
type: datetime
description: when did work begin on the document

title: published
type: datetime
description: when was the document originally published

title: updated
type: datetime
description: when was the document last updated

title: flags
type: list
description: document status e.g. hidden, draft, unlisted

title: tags
type: list
description: keywoards, categories, and tags

```

<figcaption>frontmatter.rec</figcaption>
</figure>

This is the minimum.
Plenty of projects have unique specific metadata.
e.g. 'rating' for a review site.

Usage:

<figure>

```sh
printf "%s\n" "---" $(recsel frontmatter.rec \
| recfmt '{{title}}:
') "---" \
> document.md
```

<figcaption>using frontmatter.rec</figcaption>
</figure>

Feel free to pop that into `~/bin/` or something.
Maybe replace `document.md`
with `$*`.

And the resulting document boilerplate:


<figure>

```
---
title:
description:
author:
version:
created:
published:
updated:
flags:
tags:
---
```

<figcaption>document.md</figcaption>
</figure>

This is a result
of me trying to standardize my document metadata
across multiple projects,
and noticing which individual fields feel the most reusable,
and what combination of them make the metadata feel most complete.

That's all!
Enjoy!

Resources

- https://fff.js.org/version/1.2.html
- https://indieweb.org/principles
- https://gohugo.io/content-management/front-matter/
- https://jekyllrb.com/docs/front-matter/

changequote`'dnl change quotes `back to default'
include(src/footer.html)
