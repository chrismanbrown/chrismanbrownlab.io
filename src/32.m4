define(__timestamp, 2022-02-03)dnl
define(__title, `Let`'s read: Structure and Interpretation of Computer Programs')dnl
define(__subtitle, `Introduction')dnl
define(__keywords, `book, lets-read, sicp, complexity')dnl
define(__id, 32)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

This is the first entry in the *Let's Read SICP* series.

[index](31.html) | [next](33.html) »

---

I've always wanted to read *Structure and Interpretation of Computer Programs*. It's kind of a magical looking book, with the wizard on the front pondering his orb, and the flash of lambda frightening the student next to the table with the dragon's foot.

It has a reputation as one of the best programming books of all time. I'm intrigued that it uses Scheme to teach its lessons.[^1]

[^1]: It uses Scheme for good reason. It has practically no syntax to speak of. The authors say in the Preface that the language is never formally taught. The students manage to just pick it up after a few days. They liken it to chess: you demonstrate a few basic rules and the magic and the complexity unfolds from there. I imagine that Lisp's single data structure, the list, is also an advantage in this teaching environment. Lastly, the prefaces make several references to the necessity of creating language (or <abbr title="Domain Specific Language">DSL</abbr> as we would probably call it today), which I understand Lisp to be particularly adept at. Since, again, it has basically no (and extremely regular) syntax, parsing the language should be much more simple than doing the same in other languages.

The book has been in the news[^2] lately because the new upcoming edition ditches Scheme (the dialect of Lisp used in the book since 1980) in favor of JavaScript. This has ruffled the feathers of many. Mainly those who enjoy complaining about the ubiquity of JavaScript, which is fine enough I suppose as far as hobbies go. But really you might as well complain that the ocean is a tad too salty. JavaScript is the One True Language, and it has conquered the world.


[^2]: ...for certain, narrow definitions of "the news". For example, you won't have seen this on CNN or anywhere the mainstream gets their news. I don't know why I even bother to elaborate on this here, other than to briefly reflect on the carving of deep, narrow niches, and how prone we are to dive deeply into them.

    I can't find it online anywhere any longer, but there was a proto-meme years and years ago about a good-natured, mild-mannered fellow who just so happened to enjoy himself a little bit of mustard now and then on his sandwich. When he got his first computer and went online for the first time (this was naturally before everybody was born already online, with an email address and a facebook page filled out right there on their birth certificate) he decided to see if there was a group for mustard enthusiasts on his local bulletin board server. Well, indeed there was, and he soon became an active participant in the community, and quickly became entrenched and embroiled in the politics of the group, and he slowly formed opinions which, once formed, he feverishly and fanatically defended. Both villainizing his most hated mustards (and the monsters who enjoyed them), and also defending the virtue of his favorite mustards became a moral obligation. He became radicalized. All of which is to say that there is no niche small enough or interest seemingly benign enough that there does not exist an online community for it which can sow discord and divisiveness thanks to human passions.

That said, I don't want to read about it in SICP.

So I pulled down off the shelf the Second Edition copy I bought last year and haven't even opened yet, and decided to give it a go.

The time is now. The moment is here. And you have chosen to go on this journey with me? Very well. Take my hand. Grab a jacket just in case, you can tie it around your waist.

I read all the front matter this morning, the table of contents, the forward, preface to the second edition, preface to the first edition.

There are five chapters of about 120 pages each. If I work diligently at it and cover a mere 20 pages per day, I should be able to cover a chapter a week and complete the book in five weeks. Even if the exercises are especially challenging, I expect I'll be able to manage them and 20 pages of reading. (Note to future self: this is where you can come back and laugh at my hubris when this turns out to be far more challenging than I anticipated.)

Here are a few quotes that I particularly enjoyed.

> Our traffic with the subject matter of this book involves us with three foci of phenomena: the human mind, collections of computer programs, and the computer.

These are indeed the three foci in fact of all human endeavors: the human mind conceives of an idea, a thought, a feeling, an emotion. Then one creates some form of mental model or represention of it. In this case the computer program. And then that model is applied to some medium, some hardware. I think that is how all art is created.

> More than anything else, the uncovering and mastery of powerful organizational techniques accelerates our ability to create large, significant programs.

I find this to be true. When things (concepts, items, etc.) are organized and tidied up, they are more accessible and easier to reason about, and your system becomes less complex. On the surface at least. And [reducing complexity is of course the most worthwhile pursuit](4.html).

> First, we want to establish the idea that a computer language is not just a way of getting a computer to perform operations but rather that it is a novel formal medium for expressing ideas about methodology. Thus, programs must be written for people to read, and only incidentally for machines to execute. Second, we believe that the essential material to be addressed by a subject at this level is not the syntax of particular programming-language constructs, nor clever algorithms for computing particular functions efficiently, nor even the mathematical analysis of algorithms and the foundations of computing, but rather the techniques used to control the intellectual complexity of large software systems.

I love this. Let this be the thesis of the book, the focus of the content, and I will love this text.

Two very important points here.

First, that programs are meant to be read by humans and only incidentally run by computers is a truth that intellectually I know, but of which in practice I often have to remind myself. For example, during code reviews I am frequently tempted to suggest a more terse expression because, I suppose, brevity suggests cleverness and who doesn't want to feel clever. But brevity is also often less legible. And common, shared language is always more widely understood than specialized jargon.[^5]

[^5]: One specific example I'm thinking of here is the javascript developer who consistantly wrote `for` loops instead of using language features like `Array.prototype.forEach`. In that case, it didn't actually matter, and I actively chose not to care.

Second, again with managing complexity. This is a soapbox for me. Almost all human endeavors are attempts at managing complexity. Learning syntax is the work of the novice. Learning to build large programs and structures will keeping complexity at a minimum is the work of the architect, the engineer.

> These skills are by no means unique to computer programming. The techniques we teach and draw upon are common to all of engineering design. We control complexity by building abstractions that hide details when appropriate. We control complexity by establishing conventional interfaces that enable us to construct systems by combining standard well-understood pieces in a "mix and match" way. We control complexity by establishing new languages for describing a design, each of which emphasizes particular aspects of the design and deemphasizes others.

Two things here.

First, again with controlling complexity. I won't say any more about it here other than that I continue to agree with its importance.

Second, it is interesting to point out that the author's instruction draws from the well of "all engineering design." I feel as though it's kind of popular right now to argue about whether we (software engineers) are "real" engineers or not.[^4]

[^4]: I tend to believe that no, we are not. I'm more likely to think of myself as an information architect than an engineer of software. Also, the term suggests a level of precision, certification, licensing, and accountability that is by and large absent in our industry. Also don't most engineering practices have unions or guilds to protect the craft and its practicioners from exploitation? I'd love love love the creation and widespread adoption of a Web Workers Union so we could all band together and decide as a people and a society to stop making pop-ups and stop implementing dark patterns. What a utopia we would live in then!

> Underlying our approach to this subject is our conviction that "computer science" is not a science and that its significance has little to do with computers. The computer revolution is a revolution in the way we think and in the way we express what we think. The essence of this change is the emergence of what might best be called *procedural epistemology*--the study of the structure of knowledge from an imperative point of view, as opposed to the more declarative point of view taken by classical mathematical subjects. Mathematics provides a framework for dealing with notions of "what is." Computation provides a framework for dealing precisely with notions of "how to."

This starts strong and goes in a curious direction for me.

I've heard the introductory sentence said before. "Computer science has little to do with science, or computers." I believe it has much more to do with (sorry to feed a fed horse) organizing complexity so that we can express our thoughts and describe our systems as correctly as possible. I also think this circles back to the three foci: training our human brains to create useful models that we can represent in some kind of physical form.

The curious ending is distinguishing procedural programming ("How to") from declarative mathematics ("What is"). Which I suppose is entirely true applied at this level. But I'm used to seeing this distinction made internally, within the world of programming, to distinguish procedural object-oriented programming ("How to") from declarative functional programming ("What is"). Although I suppose the distinction might be the same given that the FP fundamentalists want programming to be all mathematical functions and lambda calculus anyway.

In conclusion, the introduction to this text was flowery and inspiring, and I am excited to continue on to chapter one and take the first bites out of this big meal.

changequote`'dnl change quotes `back to default'
include(src/footer.html)
