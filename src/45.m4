define(__timestamp, 2024-03-02)dnl
define(__title, `summary of my recent job search')dnl
define(__subtitle, `most places will not hire you but at least one will and ghosting is way more common than it ought to be')dnl
define(__keywords, `interviewing')dnl
define(__id, 45)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

## Contents

1. [Introduction](#introduction)
2. [Every application and its outcome](#every-application-and-its-outcome)
3. [Tools, Process, and Tips](#tools,-process,-and-tips)
4. [Conclusion](#conclusion)

## Introduction

I lost my job in December 2023.
I started looking for a new job on December 18.
(At least, that's when I started taking notes on my search.)
And 71 days later I accepted an offer on February 27.

I had a few advantages while looking.
I am very lucky
that my partner's income
can support us both 
with very little change in lifestyle.
I also got three months of severance pay
from my former employer.
This security
was a huge help for my mental health
during this process,
because I never felt
any high pressure or any rush.

Nonetheless,
I took most of Christmas and New Years
to feel sorry for myself
and to dread starting
the job search process in earnest.
It is difficult for me
not to take rejections personally.
And I get really nervous
during coding interviews.
And I find it draining
to meet tons of new people
and be super charming
and super knowledgable
and talk about myself.

But I resolved to do at least one thing a day
in service of finding a new job.
And for the most part
I was able to do that!
It's not that high of a bar.
And once I got started,
I was somehow able to convince myself
that I actually enjoyed the whole process.

I was able to do this
because yoga.

One of the teachings of yoga
is non-attachment to outcome.
Everybody comes to the practice
with different bodies and different abilities.
And it's okay if you have a goal
to be able to touch your toes
or to do a handstand.
But the best thing you can do
it to forget your goal
and just do the work.
Every day.
And maybe eventually you'll attain your goal.
But maybe you won't.

This idea is straight from the Bhagavad Gita:

> You have a right to perform your prescribed duties, but you are not entitled to the fruits of your actions. Never consider yourself to be the cause of the results of your activities, nor be attached to inaction. 
> 
> <cite>Bhagavad Gita: Chapter 2, Verse 47</cite>

So after meditating on this
I was able to shift my focus
from "finding a job"
to meeting lots of smart people
and learning about what they're working on
and talking about code
and my experiences
and what I want to be doing next.
This was very liberating!

I sent out a lot of resumes
and filled out a lot of applications
even though I have never gotten a job by doing this.
I have only ever gotten a job
by knowing somebody at the company,
or by being recruited directly.
Or,
when I was contracting,
by working with a recruiting agency
that placed me.

See:

<figure>

Previous Job    How I Got That Job
-------         ---------------
1               🤗 Knew someone
2               🤗 Knew someone
3               🫵 Recruited
4               🫵 Recruited
5               🤗 Knew someone

<figcaption>Figure: Previous Jobs and How I Got Them</figcaption>
</figure>

## Every application and its outcome

First some vocabulary:

🤗 know somebody:
: Knowing somebody there,
  having a referral,
  or even having worked there myself.

⛄️ cold call:
: In which I filled out an appication
  or sent out a resume
  without knowing anybody there

🤖 auto-decline: 
: I can't be sure whether I was rejected by software
  or by a human.
  Either way,
  I didn't get to talk to a person.

💻 tech screen:
: A pairing interview,
  an algorithmic interview,
  or some other kind of technical interview
 
Everything else I think
should be self explanatory.

Now then:

<style>
table tr th {
  white-space: nowrap;
  vertical-align: bottom;
}
table tr td {
  font-size: 36px;
}
</style>

::: { .companies }

<figure>

company         🤗 Know Somebody?<br>🫵 Recruited?<br>⛄️ Cold Call?                       🤖 Autodeclined?        ☎️ Phone Screen?<br>💻 Tech Screen?<br>💞 Culture Interview? 👻 Ghosted?<br>👎 Declined?<br>🎉 Offer?
-------         --------------                                                            ------------            -----------                                                 --------------------------
1               🤗                                                                           🤖                                   
2               🤗                                                                           🤖                                   
3               🤗                                                                           🤖                                   
4               🤗                                                                           🤖                                   
5               🤗                                                                           🤖                                   
6               🤗                                                                           🤖                                   
7               ⛄️                                                                           🤖                                   
8               ⛄️                                                                           🤖                                   
9               ⛄️                                                                           🤖                                   
10              ⛄️                                                                           🤖                                   
11              ⛄️                                                                           🤖                                   
12              ⛄️                                                                           🤖                                   
13              ⛄️                                                                           🤖                                   
14              ⛄️                                                                           🤖                                   
15              🤗                                                                                                                                                               👻
16              🤗                                                                                                                                                               👻
17              🤗                                                                                                                                                               👻
18              🤗                                                                                                                                                               👻
19              🤗                                                                                                                                                               👻
20              ⛄️                                                                                                                                                               👻
21              ⛄️                                                                                                                                                               👻
22              ⛄️                                                                                                                                                               👻
23              ⛄️                                                                                                                                                               👻
24              ⛄️                                                                                                                                                               👻
25              ⛄️                                                                                                                                                               👻
26              ⛄️                                                                                                                                                               👻
27              ⛄️                                                                                                                                                               👻
28              ⛄️                                                                                                                                                               👻
29              ⛄️                                                                                                                                                               👻
30              ⛄️                                                                                                                                                               👻
31              🤗                                                                                                 ☎️💻💞                                                          👻                 
32              🤗                                                                                                ☎️                                                             👎         
33              🫵                                                                                                ☎️                                                             👎                         
34              🫵                                                                                                ☎️💻💞💻💻💞                                                 👎          
35              🫵                                                                                                ☎️                                                             👎           
36              🤗                                                                                                ☎️                                                             👎            
37              🤗                                                                                                ☎️💻💞                                                         🎉

<figcaption>Figure: Every application and its outcome</figcaption>
</figure>

:::


Observations:

1. Cold calls:
   applying somewhere when you don't know anybody there
   gets you nowhere.
   I didn't get a single call back from anywhere I didn't already have an in.
   Firing off resumes may feel productive.
   It did to me!
   And it felt really good to apply to moonshot places,
   or to a company you admire,
   or whose product you use and enjoy.
   But it doesn't do anything for you.
   Keep building your network!
   Because that's the only thing that works.

2. But not always!
   A lot of places where I have a good referral
   *and even places where I used to work
   and left on good terms* ended up auto-declining me
   or even ghosting me.
   This was very surprising!

3. That one place that took me all the way through
   the entire interview process
   to final rounds
   and then ghosted me?
   Totally not cool.

4. The interview process is remote.
   I never did one in-person interview.
   (And the position I accepted is hybrid on-site!)

## Tools, Process, and Tips

- Resume writing: one thing I've always wanted to have
  was a plain-text resume that I can work on and update
  without having to think about presentation and layout at all.
  A long time ago,
  I did this with https://jsonresume.org/.
  This time,
  I made my own resume.toml
  and used groff to make PDF and HTML (and plain text) versions of it:
  https://github.com/chrisman/resume.toml.

- Resume scanning:
  I was alarmed at the initial number of auto-declines I was getting.
  So I used a resume scanning service to highlight anything
  that hiring software might ding me for: https://www.jobscan.co.
  I made a few changes based on its suggestions.
  I don't know if it truly helped or not.
  But it helped me feel a little bit more in control.

- Application tracking:
  I feel like everybody has their own way of tracking their applications.
  I see lots of people use a spreadsheet like google docs.
  The last time I went through this exercise,
  I used a trello board.
  This time,
  I just kept all my notes in a rec file.
  This allowed me to keep lots of notes with each record
  and also to query my records.
  For example,
  I made frequent use of a script to show me "stale" applications:
  records that hadn't been updated in a week.

- Use your friends:
  The offer I ended up accepting
  came from an old colleague reaching out to me
  about an opening at their job.
  Build your network!
  I also made it an exercise to think about managers and colleagues
  who I enjoyed working for and with,
  and reaching out to them to say
  that I enjoyed working for and with them.
  And hey, are they trying to fill any roles right now?
  I also had several very generous friends
  critique my resume for me
  and do practice interviews with me.

## Conclusion

Interviewing is kind of weird
because you get to meet a lot of people
but also it is kind of a lonely experience
because you get rejected a lot.

When somebody is into you,
you'll know it
because they won't ghost you.
In fact they'll do the opposite of ghost you.
They'll alive you
and keep in touch
and let you know how things are going.

Hopefuly I won't have to do this again for a long time.
And if you're going through a job search
of your own right now,
let me know if I can introduce you to anybody on linkedin
or if you want me to review your resume!

changequote`'dnl change quotes `back to default'
include(src/footer.html)
