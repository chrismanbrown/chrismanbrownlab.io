define(__timestamp, 2024-08-23)dnl
define(__title, `Against Cynicism')dnl
define(__subtitle, `Toward Compassion')dnl
define(__keywords, `philosophy')dnl
define(__id, 53)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

I don't really know what to do with this post
but it keeps rattling around inside my head
so I need to exorcise it
and put it into words
and put it to rest.

I keep seeing what I think of
extreme cynicism in my online social circles.
On the fediverse,
in my rss blog feeds,
in some irc channels.
What it looks like when I see it
is somebody categorically panning
an entire technology
or discipline
or language
or platform.
Usually in the form of
hot takes
or rants.
Web dev is crap.
Javascript is crap.
C is crap.
TDD is crap.
Free software is crap.
Corporate software is crap.
This protocol is crap.
That platform is crap.
Computers are crap.
The Internet is crap.

There are a lot of things
that might lead to
this kind of hot take:
feelings of inferiority
or insecurity,
a past negative experience,
feeling overwhelmed.

And there are a lot of purposes
that this kind of opinion
may serve
for the opinion haver:
security and safety,
looking or feeling smart.

So I'm not saying that these opinions are necessarily wrong
or worthless.
But I do think that,
whatever their origin
and whatever their purpose,
they are rooted in a kind of cynicism.
And that,
more often than not,
this cynicism is harmful
for both the opinion haver
and for bystanders
who are subjected to that opinion.

Cynicism,
pessimism and contemptuous distrust,
is an easy trap
to fall into.
It is a kind of mental and emotional disease,
a fire that will burn ever hotter
the more one feeds it.

And cynicism lazy.
For once one dismisses an entire technology
(or whatever one chooses to be cynical about)
then one has absolved themself
from ever having to understand it
or find the good in it.
Cynicism is a path of least resistance.
It is passive and inactive.

It is much harder
and more virtuous
to be hopeful and optimistic.
Hope and understanding and empathy
are active pursuits.
It can be hard work
to find the good
in something one fears
or doesn't understand,
or that has otherwise been uncomfortable in the past.

And here's the thing
about these specific hot takes
and this specific brand of cynicism.
Ultimately,
these things are all made by people.
And people are good.
No person or group of persons
is setting out to make something bad.

Once capitalism gets its hooks deep into something,
a product or technology,
then yes,
the purpose of that thing
becomes to make money for investors
at all costs,
even if it causes harm.
For example,
I'm pretty comfortable
saying facebook is bad.
But even so,
there are people working on facebook,
and people are good.
I believe that there are people
at facebook
who are trying hard
to un-cordyceps that shambling beast,
and who value people and non-harm
over profits.

And that is the exercise I think.
To find a crack in one's cynicism.
(That's how the light gets in.)
A crack through which one can identify
with the people behind the thing.
And to empathize,
and to hope for the good.
Or even better,
to help work for the good.

It may be helpful
to return to the origin of one's cynicism
in order to root it out.
If one's cynicism derives from a deep insecurity,
then perhaps the source is oneself.
Or perhaps another
who caused feelings of insecurity
by means of an embarrassment.
Once identified,
it may be helpful and beneficial
to include the source of one's cynicism
into a practice
of metta or loving-kindness meditation:
once a week,
spend some time wishing them
health, safety, comfort, and happiness.

Cynicism is lazy, passive, and hungry.
It can consume the cynic, and poison them
and those around them.
Optimism and hope and compassion,
by contrast,
are often difficult work.
But are much more valuable
and important
to one's mental and social health.

So stop fanning the flames of hate
and get out there
and spread the love.

changequote`'dnl change quotes `back to default'
include(src/footer.html)

