define(__timestamp, 2024-01-30)dnl
define(__title, `Some books I read in 2023')dnl
define(__subtitle, `do ya like books I like books')dnl
define(__keywords, `books, review')dnl
define(__id, 40)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

## Contents

1. [Introduction](#introduction)
2. [Best Fiction](#best-fiction)
3. [Best Fantasy](#best-fantasy)
4. [Best Mystery](#best-mystery)
5. [Best Gamebook](#best-gamebook)
6. [Best Graphic Novel](#best-graphic-novel)
7. [Best Memoir](#best-memoir)
8. [Best Non-Fiction](#best-non-fiction)
9. [Conclusion](#conclusion)

## Introduction

What's up book nerds, this is my fifth annual year-in-books review post.

Previously:
[2022](36.html)
[2021](27.html)
[2020](21.html)
[2019](2.html)

According to goodreads, I read 32 books this year.

- https://www.goodreads.com/user/show/409485-chrisman

Yes, I'm still on goodreads, despite attempts to try something new like The Story Graph which allegedly has better superior recommendations, or Bookwyrm which is part of the decentralized "fediverse" network.

- https://app.thestorygraph.com/
- https://bookwyrm.tilde.zone/

Maybe 2k24 will be the year one of these alternatives stick.
Or better yet, maybe I'll just start keeping my own local logs and reviews,
and then periodically sync to these services.

Anyway, here are some highlights from a year in reading!

## Best Fiction

Each of these were hauntingly beautiful, quietly disturbing

- [The Nix, Nathan Hill][nix]: Either I'm suffering recency bias, or this is the best book I read in 2023. The sprawling tale of an estranged mother and son that spans decades. All about loss and yearning and love and how it damages people.

- [The Immortalists, Chloe Benjamin][imm]: A book about how the promise of death twists the lives of a group of siblings. Is there such a thing as fate? Or do we trick ourselves into playing into its expectations of us?

- [Tomorrow, and Tomorrow, and Tomorrow, Gabrielle Zevin][tom]: Another tale of pain and love as two best friends spend a lifetime in each others orbits.

- [The Sentence, Louise Erdrich][sen]: A haunting. But is it really a ghost, or is it the echo of systemic racism and violence sounding through the ages? At times absurd and silly, and heartbreakingly tragic.

[nix]: https://www.goodreads.com/book/show/28251002-the-nix
[imm]: https://www.goodreads.com/book/show/30288282-the-immortalists
[tom]: https://www.goodreads.com/book/show/58784475-tomorrow-and-tomorrow-and-tomorrow
[sen]: https://www.goodreads.com/book/show/56816904-the-sentence

## Best Fantasy

- [Dune Messiah (Dune, #2), Frank Herbert][dun]: The characters of Dune were so superpowered by the end of the first book that I had no idea and honestly little interest what Herbert was going to do with them in Dune #2. Turns out I was wrong to worry. He handled it masterfully. In fact, the second book is even better than the first!

- [Guards! Guards! (Discworld, #8; City Watch, #1), Terry Pratchett][grd]: Nobody does it better than Sir Terry. His love of language and quirky humor make every sentence a gift. I would read the operating instructions for a microwave oven if Pratchett wrote it.

- [Legends & Lattes (Legends & Lattes, #1), Travis Baldree][lat]: Light, feel-good, queer, cozy fantasy. I actually just saw the prequel at Costco! Good job, Travis!

[dun]: https://www.goodreads.com/book/show/44492285-dune-messiah
[grd]: https://www.goodreads.com/book/show/64216.Guards_Guards_
[lat]: https://www.goodreads.com/book/show/61242426-legends-lattes

## Best Mystery

- [The Village of Eight Graves (Detective Kosuke Kindaichi, #3), Seishi Yokomizo][grv]: Not my usual genre, but this was a romp. Tropey, cringey, fast, entertaining, varietally correct murder mystery in a small Japanese village. One of the most interesting parts is that the Columbo-esque detective is hardly in this book! He pops in now and again, and finally at the end to do the big reveal.

[grv]: https://www.goodreads.com/book/show/56887117-the-village-of-eight-graves

## Best Gamebook

Tabletop roleplaying is one of my primary hobbies.
And there are a lot of ways to engage with the hobby besides actually playing games.
For example, creating characters and then never playing them.
(I have about 30 unplayed characters in my D&D folder, e.g.)
Or reading gamebooks for games you never end up playing!
I read a ton of gamebooks, most of them free hobbiest publications from itch.io which aren't on goodreads.
So they aren't captured well in my end-of-year totals.
I reckon I should start keeping a log of them separate from goodreads.

- [Dungeon World, Sage LaTorra][dng]: Liberating. The Dungeon World playstyle is as cinematic as Dungeons & Dragon's is procedural.

- [7th Sea Core Rulebook (7th Sea, #1), John Wick][sea]: Shout out to the Tales of Nowhere podcast. I picked this up because they played this ruleset during their campaign. A deeply immersive setting, and a deeply irritating ruleset.

- [Sleepaway, Jay Dragon][slp]: A haunting game about some doomed campers and councilors at a dreamy summer camp. In the "Belonging Outside Belonging" tradition, this game eschews dice and skills and basically everything else that a traditional D&D gamer might expect.

[dng]: https://www.goodreads.com/book/show/17336078-dungeon-world
[sea]: https://www.goodreads.com/book/show/31205642-7th-sea-core-rulebook
[slp]: https://www.goodreads.com/book/show/54374737-sleepaway

## Best Graphic Novel

- [Saga (Volume 8), Brian K. Vaughan][sag]: The writing isn't as tight and focused as it used to be. But I will always have a soft spot for this exceptional series.

- [The Eightfold Path][bud]: A Graphic Novel Anthology, Steven Barnes: An anthology of interconnected afro-centric parables inspired by the teachings of the Buddha.

- [Tori Amos: Little Earthquakes, Tori Amos][tor]: It's the 30th anniversary of Little Earthquakes. An anthology of graphic stories inspired by the music from the album, with an introduction by her good pal Neil Gaiman.

[sag]: https://www.goodreads.com/book/show/43307890-saga-volume-8
[bud]: https://www.goodreads.com/book/show/56969416-the-eightfold-path
[tor]: https://www.goodreads.com/book/show/59465348-tori-amos

## Best Memoir

- [I'm Glad My Mom Died, Jeanette McCurdy][ded]: Hard to read. Hard to put down. Frank, matter-of-fact description of abuse and mental illness.

[ded]: https://www.goodreads.com/book/show/61917728-i-m-glad-my-mom-died

## Best Non-Fiction

- [Head Trip, Jeff Warren][hed]: A fascinating travelogue through the different stages of human consciousness.

- [How Not to Die, Michael Greger][not]: An exhaustive, scare-you-straight, steeped-in-science book about how adopting a plant based diet will literally save your life.
 
[hed]: https://www.goodreads.com/book/show/2168851.Head_Trip
[not]: https://www.goodreads.com/book/show/30225190-how-not-to-die

## Conclusion

That's it!
If you end up reading any of these,
or if you already did,
please let me know what you thought!

What were your favorite books of 2023?

changequote`'dnl change quotes `back to default'
include(src/footer.html)
