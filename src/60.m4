define(__timestamp, 2024-12-27)dnl
define(__title, `Lua is probably rated correctly actually')dnl
define(__subtitle, `Not over and not under but just right')dnl
define(__keywords, `lua')dnl
define(__id, 60)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

## Contents

1. [Introduction](#introduction)
2. [There Are Two Luas](#there-are-two-luas)
3. [Lua Is Overrated Maybe](#lua-is-overrated-maybe)
4. [Lua Is Underrated Maybe](#lua-is-underrated-maybe)
   * [The Good](#the-good)
   * [The Bad](#the-bad)
   * [The Ugly](#the-ugly)
   * [Honorable Mention](#honorable-mention)
5. [Learning Lua](#learning-lua)
6. [Not Lua](#not-lua)
7. [Conclusion](#conclusion)

## Introduction

This post is inspired by and in response to
*Lua is so underrated*
by nflatrea[^2]
because I don't think the original post
went into quite enough detail
about why Lua is great.
(Which it surely is!)

[^2]: original: https://nflatrea.bearblog.dev/Lua-is-so-underrated/  
comments: https://lobste.rs/s/jf4in1/Lua_is_so_underrated

## There Are Two Luas

When people talk about Lua
they usually talk about two different things:

1. Lua the embedded scripting runtime, and

2. Lua the language: its syntax and semantics.

## Lua Is Overrated Maybe

Lua has such a large share of the embedded scripting runtime market,
it is possible to consider it overrated in this area.
Not that I do.
But it is possible.

Caveat:
I don't really do much "systems programming."
So I haven't tried embedding a scripting language
into a compiled language before.
But my understanding from
1) effusive blog posts I've seen over the years, and
2) it's ubiquity,
is that Lua
is one of the most popular embedded scripting languages in the entire multiverse.
In fact,
if you add scripting support to your program,
the onus is probably on you
to defend NOT using Lua.

Off the top of my head,
here are a few of my favorite programs and projects
that use Lua for scripting:

- Love2d: a very popular video game framework
- Playdate: the sdk for the little yellow handheld gaming console with a crank
- pandoc: the swiss army knife of file conversion. write your own filters in Lua
- neovim/vim: uses Lua for scripting and config
- DamnSmallLinux: used Lua extensively for little desktop widgets and stuff.
  This was one of my first exposures to both linux and Lua circa 2001.
- Rockbox: alternate firmware for old ipods and mp3 players.
- OpenResty: an nginx distribution that supports scripting in Lua.
  see also: Lapis, a web application framework designed for OpenResty.
  This is the stack that https://itch.io runs on.
- Amulet: a "toolkit for experimenting with interactive graphics and audio" https://www.amulet.xyz/doc/

Honorable mention: Roblox.
I have never played roblox
but it famously uses Lua for scripting
and annecdotally I have heard of many a programmer who cut their teeth
with Lua-on-roblox.
Oh yeah, also World of Warcraft too.

More:
https://en.wikipedia.org/wiki/List_of_applications_using_Lua

## Lua Is Underrated Maybe

Lua is a great little language.
I like its syntax and design.

Here are some language features that deserve attention.

### The Good

- Tables:
  There is one data structure,
  and it is the table.
  Use it as a list or as an associative array.
  Either way,
  it is still just a table
  with the same table API.

- Concurrency:
  There is one concurrency model,
  and it is the coroutine.
  Lua does async well.
  It avoids the Color Problem[^1].
  You can yield a value from any function
  without having to decorate it,
  as you must in javascript,
  as a generator or an async function.
  And after you create or wrap or coroutine,
  you can await an async value anywhere as well.
  It just works,
  and it's really convenient and refreshing.

[^1]: https://journal.stuffwithstuff.com/2015/02/01/what-color-is-your-function/

- Metatables:
  You can change the default behavior of tables
  and do object-oriented programming
  via prototypal inheritance
  using metatables.
  Makes your single data structure a little more versatile.
  By the way,
  I think that this is one of the best little showcases
  of using class mixins in Lua:
  https://github.com/a327ex/blog/issues/2

### The Bad

- Colon notation:
  Okay there's this convention where you call a table function using dot notation
  like `rocket.blastoff(rocket, fuel, cargo)`.
  BUT,
  because it is so common,
  if the first argument to a table function is the table itself
  ('rocket' in this case)
  then you can use colon notation to just pass it implicitly:
  `rocket:blastoff(fuel,cargo)`.
  The function definition is usually something like
  `function rocket (self, fuel, cargo) ... end`
  Now,
  this is barely "Bad".
  But I am often unsure whether I should be using dot or colon notation when calling a table function.
  Especially when using somebody else's library.
  It's just something I always find myself thinking about.
  *NOTE: thanks acdw for helping me proofread and clarify this section.*

- Standard Library:
  Or rather, lack thereof!
  Lua supplies some adequate libraries for math, string, table, io, and os.
  And that's it!
  I'd say roughly 75% of the time,
  the Lua programmer will want to bring in a 3rd party library
  like lume, luafun, or penlight[^3]
  for additional features:
  This could be considered a feature:
  it keeps the runtime small.

[^3]:
  https://github.com/rxi/lume/  
  https://github.com/luafun/luafun  
  https://lunarmodules.github.io/Penlight/  


- Pattern Matching:
  Lua does not support regex.
  (See above: Standard Library.)
  Instead, the string library
  has its own limited vocabulary
  of pattern matching symbols.
  It's not terrible,
  but it's different enough to be annoying.

### The Ugly

These are things that are neither good nor bad,
but are just kinda weird about Lua.

- 1-based indexing:
  It's FINE. Get over it.

- `end`.
  Lua doesn't use curly brackets to designate scope.
  Instead you use the `end` keyword
  to close a function or a loop or a branch.
  It's fine.
  Just unusual to my mind
  and a little verbose.

- Multiple returns:
  Functions can return multiple values.
  This is mostly fine
  because it is mostly avoidable.
  Except for error handling.
  Convention for error handling is to return a value or status and maybe an error code.
  At least,
  this is the way `pcall` (or "protected call") works
  when you use it to wrap a function that can throw errors,
  and it can result in a lot of boilerplate:

  ```lua
  function throws () error("Oh no! Something bad happened!") end
  local ok, err = pcall(throws)
  if not ok then print ("Error!" .. err) end
  ```

### Honorable Mention

- luarocks:
  Remember how there's no standard library to speak of?
  Well check out the Lua package manager!
  It is fine.
  https://luarocks.org/

- LPeg:
  Remember how Lua doesn't support regex?
  Well checkout LPeg!
  A Parsing Expression Grammar for Lua.
  Which is kind of an unofficial official library.
  It's like regex, except not in the slightest.
  https://www.inf.puc-rio.br/~roberto/lpeg/lpeg.html

## Learning Lua

Want to learn Lua?
That's great!
Here's what I recommend:

- Learn X in Y minutes, where X=Lua:
  A real quick-n-dirty overview.
  https://learnxinyminutes.com/lua/

- Programming in Lua, 1st Edition:
  A free online ebook.
  It's about version 5.0,
  so it's a wee bit out of date,
  but all the fundamentals are the same.
  https://www.lua.org/pil/contents.html

- Lua 5.4 Reference Manual:
  https://www.lua.org/manual/5.4/

- lua-users.org:
  It's a wiki!
  http://lua-users.org/wiki/LuaDirectory

## Not Lua

Need Lua but want the regularity and simplicity of a lisp syntax?
Try fennel!
https://fennel-lang.org/
I love fennel in its own right
and am likely to use it anytime Lua is called for.

Need Lua but want types?
Teal is a typed dialect of Lua!
https://github.com/teal-language/tl/blob/master/docs/tutorial.md

Need Lua but wish it was TypeScript?
Try TypeScript To Lua!
https://typescripttolua.github.io/

Need Lua but wish it was a little bit more like Python?
Try Moonscript!
https://moonscript.org/

More: https://github.com/hengestone/lua-languages

## Conclusion

Lua is neat!

I don't think it is overrated at all.
But nor do I think it is really underrated either.
It's just, rated.
And that's okay!

changequote`'dnl change quotes `back to default'
include(src/footer.html)

