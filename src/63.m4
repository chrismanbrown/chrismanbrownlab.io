define(__timestamp, 2025-02-04)dnl
define(__title, `Best Books of 2024')dnl
define(__subtitle, yeah that's right I read books)dnl
define(__keywords, `books, review')dnl
define(__id, 63)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

Previously:
[2023](40.html)
[2022](36.html)
[2021](27.html)
[2020](21.html)
[2019](2.html)

New for this year
is that I'm not using goodreads
as my primary way to track books.
I am using my local `books.sqlite` database!
My goal was to have one single database
and then export to goodreads
and bookwyrm
and storygraph from there.
This has been mostly successful.

I read 46 books.
Here are some highlights.

## Fiction

- Remarkably Bright Creatures, Shelby Van Pelt:
  A cute story about family.
  And a giant octopus.
  Cute,
  despite it using one of my least favorite narrative tropes:
  big, entirely preventable misunderstandings. 
  If only people would use their words at each other!
  Read this if you want something light and cute,
  and if you like intelligent sea creatures.

- My Year of Rest and Relaxation, Ottessa Moshfegh:
  Disquieting story of a young woman
  who tries to take enough drugs
  to black out for a year.
  Read this if you want to feel uneasy and uncomfortable
  about a protagonist acting badly.

- Dead Collections, Isaac Fellman:
  A trans archivist who gets turned into a vampire
  inconveniently at the start of their transition
  tries to figure out their job and their love life.
  Read this if you want to learn about what it's like to be trans.
  Or a vampire.
  Or an archivist.


## Fantasy

- Witch King, Martha Wells:
  Totally cool fantasy,
  and also kind of a mystery.
  Read this if you want deep world building
  and discrete magic systems,
  and to read about a demon prince who is also a witch
  trying to figure out what happened to him and his friends.

## Horror

- Mongrels, Stephen Graham Jones:
  Coming of age story about a family of werewolves
  trying to get by
  on the fringe of society.
  Read this if you want to read a self-aware story about werewolves
  which is part metaphor for people being squeezed out on the margins of society,
  and is also part metaphor about growing up without any home stability.

- Our Share of Night, Mariana Enriquez:
  Coming of age story about a kid and his dad
  who are both caught up in a dangerous cult.
  Takes place in Argentina,
  translated from the Spanish.
  Read this is you want a straight up spooky horror mystery
  where the real monster is the things that the dad
  does to his son
  in the name of protecting him and keeping him safe.

- Red Rabbit, Alex Grecian:
  Weird western about cowboys and ghosts
  and witches and devils.
  Read this if you want to read about
  cowboys
  with good manners and good shootin'
  who get kind of accidentally caught up
  with hunting down a witch,
  and all of the weird weird stuff
  that happens as a consequence.


## Memoirs

These are all audiobooks,
all read by the author.
(I'm sure there are also other formats,
but that's how I read them.)

- Making It So, Patrick Stewart:
  Read the audiobook if you want to hear Stewart narrate it.
  Read this if you want to hear about Stewart growing up dirt poor
  and falling in love with stage acting.
  And cheating on all his romantic partners.
  Oh yeah he was in Star Trek
  and Dune
  and X-Men.
  But mostly he just wants to tell you how much he loves stage acting.

- Sure I'll Join Your Cult, Maria Bamford:
  Read the audiobook so you can hear Bamford expertly delivering
  her own weird brand of comedy.
  Read this if you want to hear how somebody
  with a ton of neuroses and mental illnesses
  deals with their misfiring brain.
  (It's self help programs.
  That's how she copes.
  That's where the title of the book comes from.
  She collects 12 Step programs as if they're pokemon.)

- Better Living Through Birding, Christian Cooper:
  Remember when that white lady
  called the cops on a guy in Central Park
  for birding while black?
  That was this guy!
  Read this if you want to hear
  a gay,
  black,
  harvard educated comic book writer,
  traveler,
  and lifelong birder
  talk all about birds
  also his daddy issues.

## Comics

One graphic novel,
one superhero comic,
and one revivalist comicbook.

- Ducks: Two Years in the Oil Sands, Kate Beaton:
  Read this if you want to hear 
  a true story from the author of
  *Hark! A Vagrant*
  about all of the trauma and abuse she accumulated
  over the years she spent
  working in the oil fields of rural Canada.

- Justice League vs. Godzilla vs. Kong, Brian Buccellato:
  Straight up awesome superhero nonsense.
  Read this if you want to see Godzilla
  punch the shit out of Superman.
  Or if you want to see Mecha Batman
  got toe to toe with some other kaiju.
  It is absurdly awesome.
  Absolutely ridiculous.

- The Flintstones, Mark Russell:
  A surprise hit!
  Read this if you want a very funny
  and scathingly satirical fresh take
  on everybody's favorite modern stoneage family.

## Audiobooks

Best narrator:

- The Eyes and the Impossible, Dave Eggers:
  Read by Ethan Hawke.
  A charming Animal Story
  about a wild dog who lives in a park
  and all of the stuff that happens to him.
  Hawke does a fantastic job reading Eggers.
  Don't miss this one.

## Nonfiction

- Dying for a Paycheck, Jeffrey Pfeffer:
  How workplace stress is killing us all.
  Read this if you haven't already been radicalized
  about workplace reform.

- UNIX text processing, Dale Dougherty:
  Niche selection of the year.
  Part technical manual,
  part history book
  from that particular time
  when we were transitioning from typewriters
  to computers,
  but before word processors were really a thing.
  So this is about how to do
  text processing
  and prepare your writing for print
  with groff and a little awk.


changequote`'dnl change quotes `back to default'
include(src/footer.html)


