define(__timestamp, 2024-12-27)dnl
define(__title, `text browsers i have known and loved')dnl
define(__subtitle, `in praise of lynx and w3m')dnl
define(__keywords, `terminal, browsers, lynx, w3m')dnl
define(__id, 61)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

## Content

1. [Why do a text browser?](#why-do-a-text-browser?)
2. [lynx](#lynx)
3. [links](#links)
4. [w3m](#w3m)

## Why do a text browser?

1. It's fun!

2. It allows for a very quick and seamless experience for me
   when I'm working in the terminal.
   At times,
   maybe up to 50% of my web browsing comes from terminal links
   via irc or readme files or something else.
   It is valuable to me
   to be able to quickly open up a text browser
   and quickly view the content
   without the mental cost of context switching
   or the temporal cost of waiting for firefox to open.

## lynx

My first text browser was lynx.

I like lynx.

lynx is allgedly the oldest browser still under development today.
It is also probably the first text browser I ever used.
At least,
it is the text browser I have used the longest,
and is the one with which I am most familiar.

It is good for getting a plain text dump
of a website:

```
lynx https://chrismanbrown.gitlab.io/61.html -dump | less
```

It supports gopher, netnews, and finger protocols
out of the box.

I really like the 'list' view,
where you can view just all of the links on a page
and navigate them
quickly and easily.

You can use it as a pretty decent file browser.
You can edit local documents,
create new ones,
tag files for batch processing,
etc, etc.
This is great for, e.g.,
remoting ~/public_html on a remote server
and doing a `lynx .`
to navigate and edit your html files.

It has a quaint feature where you can "send a comment to the owner of the document."
This sends an email.
But it rarely works anywhere.

It has basic bookmarking.

Its killer feature might be rudimnentary macro support via command logging and replaying
with the `-cmd_log` and `-cmd_script` flags.
I recently used this feature to automate uploading a csv of book reviews
to goodreads because it was easier than trying to figure out their auth flow with curl.
(Goodreads retiring their API continues to be one of the most vexsome examples of self-sabotage on the internet,
up there with google killing its rss reader.)

## links

I've used links a couple times.

I don't know too much about links/elinks.
But it's pretty good!

It's the menu-driven browser,
if you like that.

There's a graphical version that is really hot right now:
http://links.twibright.com/

It doesn't support javascript,
but can display inline images.
And it does a downright decent job
displaying tables and frames.
It's pretty neat!
Big fan.

## w3m

Nowadays I use w3m the most.

While I continue to be most familiar and comfortable with lynx,
w3m is quickly becoming my new favorite browser to tinker with.

w3m is a really neat browser,
and also can be used as a pager
like less(1) or more(1).

It can display graphics,
even without X running somehow.
I don't know how that works.

It supports the nntp protocol for netnews,
and can post form data from the command line like curl.

It has tabbed browsing.
It also does a really decent job rendering tables and frames.

w3m's "list view"
is even nicer than lynx's
because you can navigate with hotkeys
(one for each link)
instead of with arrow keys.

Its killer feature might be its support for local cgi scripting.
This makes it incredibly flexible and powerful!
And you can also set custom URI handlers
and rerouting based on URLs.
This combination allows you to pretty easily redirect
gemini and gopher URLs via a cgi script to web proxies.
I also have mine set up to automatically open youtube/video links
in mpv.
And links to images in an image viewer.
It's really great.

It has tight shell integration.
You can pipe page content to a program,
or program output to a page.
And you can write custom commands.

It really is the bee's pajamas!

changequote`'dnl change quotes `back to default'
include(src/footer.html)
