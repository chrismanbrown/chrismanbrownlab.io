define(__title, `Contact')
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

I would love to hear any thoughts, ideas, corrections, recommendations, jokes, or recipes
you might like to share with me.

- gmail: christopher.p.brown
- ~~twitter: @chrisman~~
- ~~mastodon: @cpb@mastodon.social~~

changequote`'dnl change quotes `back to default'
include(src/footer.html)
