define(__timestamp, 2022-02-03)dnl
define(__title, `Let`'s read: Structure and Interpretation of Computer Programs')dnl
define(__subtitle, `Series index page')dnl
define(__keywords, `book, lets-read, sicp')dnl
define(__id, 31)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

This is the index page for a *Let's Read* series in which I'll be taking notes on and discussing the classic *Structure and Interpretation of Computer Programs*.

All posts in this series:

- [Introduction](32.html)
- [Chapter 1 Part 1](33.html)
- [Chapter 1 Part 2](34.html)
- [Chapter 2 Part 1](35.html)


<br>

changequote`'dnl change quotes `back to default'
include(src/footer.html)
