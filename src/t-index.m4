include(src/header.html)
# All Tags


## object-oriented-programming 
- [Functional Factories in JavaScript](1.html)

## sicp 
- [Lets Read: Structure and Interpretation of Computer Programs](34.html)
- [Lets Read: Structure and Interpretation of Computer Programs](35.html)
- [Lets Read: Structure and Interpretation of Computer Programs](33.html)

## interviewing 
- [summary of my recent job search](45.html)
- [alien alphabet](14.html)
- [Pouring water into cups](17.html)

## biboumi 
- [how to irc over xmpp](52.html)

## xmpp 
- [how to irc over xmpp](52.html)

## recommendation 
- [zathura is a good document viewer](58.html)
- [ten years of colemak](59.html)

## python 
- [setting up python on macos](10.html)

## make 
- [Markdown and makefiles](3.html)
- [how i use Make and just for builds and tasks](51.html)

## browsers 
- [text browsers i have known and loved](61.html)

## database 
- [TTRPG and RDBMS](41.html)
- [Incremental Databases](55.html)
- [recfiles](28.html)

## wiki 
- [on bookmarks and note taking](11.html)

## retro 
- [Counting Apples](23.html)

## gitlab 
- [Moving from Github](22.html)

## webdev 
- [Try the TAP stack if you hate HTML CSS and Javascript!](48.html)

## lets-read 
- [Lets Read: Structure and Interpretation of Computer Programs](34.html)
- [Lets Read: Structure and Interpretation of Computer Programs](35.html)
- [Lets Read: Structure and Interpretation of Computer Programs](33.html)

## data 
- [Visualizing Data with Sparklines](43.html)

## zsh 
- [zsh suffix alias](8.html)

## lua 
- [Lua is probably rated correctly actually](60.html)

## books 
- [The Best Books Of 2022](36.html)
- [Books I Read in 2021](27.html)
- [Books I Read in 2020](21.html)
- [Some books I read in 2023](40.html)
- [Books I Read in 2019](2.html)
- [Best Books of 2024](63.html)

## vim 
- [vimwiki](30.html)
- [expressions with line numbers in vim](20.html)
- [Registers](19.html)

## builds 
- [how i use Make and just for builds and tasks](51.html)

## markdown 
- [table markup ranked worst to best](62.html)
- [frontmatter.rec](50.html)
- [templating markdown+yaml frontmatter with rec and awk](49.html)

## awk 
- [marking up abbreviations with awk](47.html)
- [templating markdown+yaml frontmatter with rec and awk](49.html)

## just 
- [how i use Make and just for builds and tasks](51.html)

## mixins 
- [Functional Factories in JavaScript](1.html)

## yaml 
- [templating markdown+yaml frontmatter with rec and awk](49.html)

## culture 
- [place attachment](13.html)

## guide 
- [how to irc over xmpp](52.html)
- [How to theme a website](16.html)

## keyboard 
- [ten years of colemak](59.html)

## sandwiches 
- [Functional Factories in JavaScript](1.html)

## sqlite 
- [Incremental Databases](55.html)

## bookmarks 
- [on bookmarks and note taking](11.html)

## metadata 
- [frontmatter.rec](50.html)

## gadgets 
- [Review: Freewrite Traveler](29.html)

## recutils 
- [templating markdown+yaml frontmatter with rec and awk](49.html)
- [Re: Guitar Driven Development](57.html)
- [TTRPG and RDBMS](41.html)
- [Incremental Databases](55.html)
- [frontmatter.rec](50.html)
- [recfiles](28.html)

## brief 
- [Pickles](26.html)

## groff 
- [Groff II](42.html)
- [Groff I](38.html)

## communication 
- [how to use slack](15.html)

## classes 
- [Functional Factories in JavaScript](1.html)

## ci 
- [Github CI](5.html)

## vue 
- [i got locked out of building my static website](37.html)

## nextjs 
- [making a static website with nextjs and contentful](24.html)

## review 
- [Best Books of 2024](63.html)
- [The Best Books Of 2022](36.html)
- [Books I Read in 2021](27.html)
- [Books I Read in 2020](21.html)
- [Some books I read in 2023](40.html)
- [Books I Read in 2019](2.html)
- [Review: Freewrite Traveler](29.html)

## philosophy 
- [Against Cynicism](53.html)
- [Reducing Complexity](4.html)

## m4 
- [table markup ranked worst to best](62.html)
- [m4](9.html)
- [Re: Guitar Driven Development](57.html)

## unix 
- [Markdown and makefiles](3.html)

## writing 
- [frontmatter.rec](50.html)

## node 
- [Top Ten Things I Like About Deno](25.html)
- [i got locked out of building my static website](37.html)

## deno 
- [Top Ten Things I Like About Deno](25.html)
- [Re: Making a single-file executable with node and esbuild](39.html)

## ssg 
- [making a static website with nextjs and contentful](24.html)

## nuxt 
- [i got locked out of building my static website](37.html)

## github 
- [Moving from Github](22.html)
- [on bookmarks and note taking](11.html)
- [Github CI](5.html)

## complexity 
- [phone numbers](12.html)
- [Lets Read: Structure and Interpretation of Computer Programs](33.html)
- [Lets Read: Structure and Interpretation of Computer Programs](34.html)
- [Lets Read: Structure and Interpretation of Computer Programs](35.html)
- [Reducing Complexity](4.html)

## ssh 
- [Github CI](5.html)

## slack 
- [how to use slack](15.html)

## markup 
- [table markup ranked worst to best](62.html)

## smolnet 
- [Smol Finger Client](54.html)
- [Web Applications and Web Documents](56.html)

## react 
- [making a static website with nextjs and contentful](24.html)

## book 
- [Lets Read: Structure and Interpretation of Computer Programs](34.html)
- [Lets Read: Structure and Interpretation of Computer Programs](35.html)
- [Lets Read: Structure and Interpretation of Computer Programs](33.html)

## irc 
- [how to irc over xmpp](52.html)

## mentoring 
- [mentoring](6.html)

## racism 
- [Racism and Oppressive Language in Software Development](7.html)

## note-taking 
- [on bookmarks and note taking](11.html)
- [vimwiki](30.html)

## lynx 
- [text browsers i have known and loved](61.html)

## learning 
- [on bookmarks and note taking](11.html)

## functional-programming 
- [Functional Factories in JavaScript](1.html)

## design 
- [Reducing Complexity](4.html)

## finger 
- [Smol Finger Client](54.html)

## terminal 
- [text browsers i have known and loved](61.html)

## potpourri 
- [how to name a computer](18.html)
- [Pickles](26.html)
- [Web Applications and Web Documents](56.html)
- [doing a blog](46.html)
- [ten years of colemak](59.html)

## forth 
- [Counting Apples](23.html)

## w3m 
- [text browsers i have known and loved](61.html)

## pandoc 
- [How to create a PDF archive of some blog posts with pandoc](65.html)

## javascript 
- [Try the TAP stack if you hate HTML CSS and Javascript!](48.html)
- [Re: Making a single-file executable with node and esbuild](39.html)
- [phone numbers](12.html)
- [scope](44.html)
- [Top Ten Things I Like About Deno](25.html)
- [Functional Factories in JavaScript](1.html)
