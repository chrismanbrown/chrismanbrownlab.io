define(__title, `uses') 
define(__subtitle, `Stuff I Use')
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

## Contents

1. [About](#about)
3. [Development](#development)
4. [Terminal Apps](#terminal-apps)
5. [Desktop Apps](#desktop-apps)
6. [Webapps](#webapps)
7. [Gear](#gear)
8. [iOS Apps](#ios-apps)

## About

I keep this page because I think Uses pages
are intrinsically interesting to read.
And also,
over the long term,
I am interested in seeing how my
preferences and tools
change over time.

Changelog:

- 2025-02-03 - new year new yuses
- 2024-08-03 - update
- 2020-01-10 - first post

See also:
<https://github.com/wesbos/awesome-uses>

## Development

EDITOR

: 2024: neovim is still the best

: 2020: neovim

MULTIPLEXER

: 2024: Still tmux. Briefly flirted with dvtm + dtach (on alpine), which is great if you want the smallest number of features.  And I actually do use abduco + dvtm on my raspberry pi home server because it's super lightweight. But tmux is still the greatest. (dvtm is pretty nice though honestly.)

: 2020: tmux

SHELL

: 2024: zsh + ohmyzsh. I use bash here and there. People seem to really like fish. But I don't feel any real desire to change.

: 2020: zsh + [ohmyzsh][1]

TERMINAL

: 2025: Ghostty. Installed it on a lark, and never found any reason to switch back. Except it doesn't support sixel. But I don't really miss it I guess. Honestly relieved to be away from iterm2 and its weird AI stuff.

: 2024: Macbook: [iterm2](https://iterm2.com/). Started using it bc sixel support. Almost quit it because they started to put AI into it by default. But then they hid it behind a config option. But I'm watching you, iterm.

: 2024: Linux: Konsole. It's what came with my KDE Plasma desktop. It's fine. On my wayland sway desktop, Foot.

: 2020: Terminal.app. People seem to really like iterm panes and tabs and windows. But I use tmux, so I see no need to switch for that. Terminal.app is just fine.

TERMINAL COLORS

: 2024: [synthwave alpha](https://github.com/mbadolato/iTerm2-Color-Schemes/?tab=readme-ov-file#synthwave-alpha)

: 2020: [birds of paradise][3] (or something else from that repo)

VIM COLORS

: 2024: [vim-kalisi](https://github.com/freeo/vim-kalisi?tab=readme-ov-file#vim)

: 2020: none, actually. I just `set background=dark` and leave it with the defaults, believe it or not.

FONT

: 2024: [comic mono](https://dtinth.github.io/comic-mono-font/) [nerdfont patched](https://www.nerdfonts.com/). I don't miss ligatures. And I really like the personality of comic mono.

: 2020: [Fira Code][4]. mmmmm, ligatures

[1]: https://ohmyz.sh/
[2]: https://github.com/chrisman/dotfiles
[3]: https://github.com/lysyi3m/macos-terminal-themes?tab=readme-ov-file#birdsofparadise-download
[4]: https://github.com/tonsky/FiraCode/

LANGUAGES

: 2024: For small scripts and applications, either fennel (fun, expressive, infiltration) or typescript + deno (familiar, universal). Both produce convenient binaries. recfiles are the best database for small jobs, followed by sqlite. rec and m4 and mustache templates are best. groff for typesetting. This should probably be its own h2 level section..

## Terminal Apps

IRC: weechat

BROWSER: w3m, lynx

CALENDAR: remind(1)

RSS

: 2025: sfeed_curses + random stuff to parse sfeed feeds files.
  newsboat kind of stopped working for reasons I haven't been bothered to investigate that hard.

: 2024: newsboat.
  After using tinytinyrss (web) and miniflux (web),
  I've switched full time over to newsboat.
  Pros: endlessly configurable.
  Cons: can't sync bt devices. (no feeds on the go)

: 2024: sfeed.
  sometimes
  I like to throw a small sfeed static export
  up on a web server.
  sfeed_curses is actually really nice.

PODCASTS: 

: 2025: a weird cludge of sfeed | fzf | mpv

: 2024: PODBOAT + MPV

## Desktop Apps

WEB BROWSER

: 2024: Still Firefox.
  I've straight up deleted Chrome at this point.
  I use Safari as my "other browser" when I need to.
  If I don't want to fire up an OS sized application,
  then links2 or dillo.

: 2020: Firefox

BROWSER EXTENSIONS (THE ABSOLUTELY NECESSARY ONES)

: [vimium][5]: navigation and more. Since 2020

: keepassxc-browser: passwords. Since 2020

: [ublock-origin][6]: ad blocker. Since 2020

: Dark Reader: for sites that don't have dark mode. Since 2024

: <del>User-Agent Switcher and Manager: to spoof my OS / browser so I can download audiobooks from overdrive.com</del> overdrive dropped support for downloading audiobooks, but I can still download the .odm file with a clever little bookmarklet and do all the license fulfillment and file downloading with a little bash script.

: Redirector: for redirecting medium to scribe, youtube to invidious, etc. Since 2024

SMOLNET BROWSER

: 2024: Lagrange for gemini, gopher, finger

PASSWORDS

: 2020: keepassxc, but also flirting with pass

: 2020: [keepassxc][7]

COMMUNICATION

: 2024: Discord for a lot of ttrpg stuff

: 2024: MS Teams for work: liked it better than slack!

: 2020: Slack

SYNCING

: 2025: Messed around with Syncthing and it works okay. Still depend on dropbox more than I would like to

: 2024: Dropbox scaled back the number of devices they support. I still use it for syncing my keepassxc file between my laptop and my phone, but nothing else. Otherwise I use ssh and rsync and nc and AirDrop.

: 2020: Dropbox: I keep a few files synced across a few devices. Chiefly my keepass
  file. Vaguely dissatisfied with Dropbox as a solution at the moment because
  of its device limitations. Perhaps I can use Keybase?

WINDOW MANAGER

: 2024: [Rectangle](https://rectangleapp.com/) is the successor to Spectacle.

: 2020: [Spectacle][8]: the minimum acceptable amount of window management. Criminal
  that it's missing from MacOS as a default. 2020 goals: might check out
  [chunkwm / yabai][9]

CLIPBOARD MANAGER

: 2024: Flycut. What a life saver.

THUNDERBIRD: email, netnews, calendar, xmpp, irc. What even is this piece of software? It's absurd. Why does it do so much?

[5]: https://addons.mozilla.org/en-US/firefox/addon/vimium-ff/
[6]: https://github.com/gorhill/uBlock
[7]: https://keepassxc.org/
[8]: https://github.com/eczarny/spectacle
[9]: https://github.com/koekeishiya/chunkwm

## Webapps


MAIL: gmail

CALENDAR

: 2024: I don't really use a web calendar any longer. (For personal use.) I use remind(1) on the command line as my source of truth, periodically export to .ics to import into Thunderbird and into Calendar.app. Also I print out a paper calendar (at the public library) to keep on the fridge because the only person I really need to sync my calendar with also lives in my house.

: 2020: google calendar

GIT FORGE

: 2024: I don't like to use GitHub any more because of their government (ICE) contracts, breach of user licenses and trust with copilot AI, selling to Microsoft, and their monoculture / monopoly. I have some code on GitLab now, and on various gitea instances. Also I just don't publish a lot of my code anymore. Not everythig needs to be public anyway. I have a lot of my code on a private ssh server.

: 2020: GitHub? Does that count as a webapp? I spend a lot of time on GitHub.

SOCIAL MEDIA

: 2024: Mastodon is the final social platform I remain on. I ditched reddit, and twitter and insta long before that, and facebook long before that. I prefer blogs, email, and irc these days.

## Gear

WORK COMPUTER

: 2024: Macbook Pro M1: Great performance! Great battery life! An escape key! Almost restores my faith in Apple hardware.

: 2020: MacBook Pro (15-inch, 2018)

PERSONAL COMPUTER

: 2024: MacBook Pro (15-inch, 2018): bought my work computer from work. Still kind of hate the touchbar and the keyboard and siri. I'd love a decent linux laptop daily driver.

: 2024: Pinebook Pro: Cute little hobby hacking computer, currently running Manjaro + Plasma. My decent linux laptop daily driver.

: 2024: MNT Pocket Reform: So small! So cute! Debian + wayland sway

: 2024: clockworkpi uconsole: tiny handheld computer for hobbies and coding

: 2020: MacBook (12-inch, 2016):
  this tiny thing was smaller than the macbook air at the time for some reason. I loved it. It was tiny, light, and portable.

KEYBOARD

: 2024: [ergodox ez](https://ergodox-ez.com/). can't live without it. carry it back and forth between home and office.

: 2020: [kinesis freestyle2 (mac)][10]

KEYBOARD LAYOUT

: 2024: still colemak, but also now that I have a programmable keyboard, I [make my own layouts and layers](https://configure.zsa.io/ergodox-ez/layouts/pq7BP/latest/0)

: 2020: [colemak][11]. I've been colemaking for years now, and give it
  100% credit for eliminating my wrist pain. I have successfully converted
  two coworkers to fulltime colemak. Downsides: I can no longer type
  qwerty. Upsides: colemak is everywhere so I never have to type qwerty.

DESK

: 2024: I still use same desk. A slightly different version [of this one](https://www.costco.com/tresanti-geller-47%e2%80%9d-adjustable-height-desk.product.4000139627.html). It's great! I should stand up more.

: 2020: I have a nice stand up desk, a white glass-top from costco.

CHAIR

: 2024: Herman Miller Aeron. Is good chair.

: 2024: I no longer have the IKEA POÄNG and I miss it dearly. But I do have a nice, comfy vintage green armchair that I inherited when a friend moved and didn't have a space for it in her new house. I've worked in a couple different offices since my last update, and can officially decry with authority the utter dearth of cozy places from which to work in the office. As a human with a back condition, I need a place to recline a little bit and put my feet up while working.

: 2020: Herman Miller Aeron. Great ebay purchase.
  Purchased on the recommendation of my engineering manager.
  He says he makes the purchase of one a condition of
  accepting any new job.

: 2020: And an IKEA POÄNG + footstool for lounging.
  Super comfy. I spend so much of my time here.

MICROPHONE

: 2024: blue yeti nano. it's okay.

HEADPHONES

: 2025: Apple Airpod Pro 2. Finally got rid of the old airpods that sucked. These ones are awesome! Great noise cancelation. Good fit. I like the touch controles.

: 2025: acrux over the head headphones. they're great!

: 2024: Google acrux bluetooth headphones. These are excellent headphoes. Good sound, good mic, good connectivity / reliability, good noise reduction.

: 2024: Apple Airpod Pros. I hate these pods and I have for years.. They've never worked well. The left one clicks and buzzes constantly. Apple replaced them once, but then wouldn't replace them again.

WEBCAM

: 2024: logitech brio 101. The C920 stopped working. I think. I don't know, there was a shuffling of offices, and some cameras got swapped and passed around, and one of them stopped working... Anyway now I have this one. Seems to work much better than the C920. Good zoom and cropping, good in low light.

: 2020: logitech C920 HD Pro. Sure beats the laptop cam.

PHONE

: 2024: same phone

: 2020: iPhone XS. It's fine. I haven't bought a new phone in  I can't even tell you how many years. I always just get hand-me-downs from my partner, who is apparently more of a gear head than I am. I don't even like iPhones all that much. I'd rather have an Android.

CAMERA

: 2024: Argus C3 Colormatic.
  My grandfather died and now I have it.

EREADER

: 2025: Kindle Oasis -
  It's got clicky buttons!
  And a backlight and dark mode.
  Almost perfect.
  If I could only just permanently disable the touchscreen..

: 2024: Kindle 11th Gen (2022) -
  It's got a backlight.
  But that's about all it has over the K4NT.
  (And also a USB-C port. And dark mode.)
  But I hate the touch screen.
  Give me back my clicky buttons!

: 2020: Kindle 4 No Touch (2012) -
  Probably the pinnacle of ereader design.

TABLET

: 2025: this bent iPad is now an iPad with a crack clear across the screen that makes weird colors across the screen. I should upgrade.

: 2020: a bent iPad Air 2 that I use to read PDFs and CBZs. Another hand-me-down. Again my partner is the gear head and the apple fan. I far prefer my old beloved Nexus 7. it was the perfect tablet.

[10]: https://www.amazon.com/Freestyle2-Ergonomic-Keyboard-Standard-Separation/dp/B00CMALD3E
[11]: https://colemak.com/
[12]: https://www.amazon.com/Ikea-Armchair-Footstool-Machine-Washable/dp/B0069B1KTO/ref=sr_1_5?crid=1ECMMT9ODUC05

## iOS Apps

SHELL

: 2024: iSH - a whole tiny alpine install!

: 2020: termux

PODCASTS: overcast.fm

AUDIOBOOKS: mp3 audiobook player

IDENTIFYING BIRD NOISES: merlin

XMPP: monal

USENET: newstaplite

RSS: netnewswire

GEMINI:

: 2025: elaho vanished fom the app store? Luckily lagrange is available through TestFlight. And lagrange is best-in-show. It's what I use as a desktop browser.

: 2024: elaho

BOOKMARKS AND READING: pocket

MASTODON: metatext

NOTES: notes.app

<p></p>
changequote`'dnl change quotes `back to default'
include(src/footer.html)
