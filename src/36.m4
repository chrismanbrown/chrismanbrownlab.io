define(__timestamp, 2022-12-31)dnl
define(__title, `The Best Books Of 2022')dnl
define(__subtitle, `books? books!')dnl
define(__keywords, `review, books')dnl
define(__id, 36)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4


## Contents

1. [Intro](#intro)
2. [Best Young Adult Fiction](#best-young-adult-fiction)
3. [Best Horror Anthology](#best-horror-anthology)
4. [Best Anarchist Sci-Fi](#best-anarchist-sci-fi)
5. [Best Fantasy](#best-fantasy)
6. [Best Computer Science Book](#best-computer-science-book)
7. [Colophon](#colophon)

## Intro

This is my fourth year-in-books review post.

Past posts:
[2019](2.html)
[2020](21.html)
[2021](27.html)

According to [goodreads](https://www.goodreads.com/user/show/409485-chrisman), this year I read 26 books.

I continue to read much less ever since the upheaval of 2020.

This year I'm not going to list every book I read, because I don't think that's interesting.
Instead, I'll just share with you the ones that I liked the best.

## Best Young Adult Fiction

[The Owl Service, Alan Garner](https://www.goodreads.com/book/show/83829.The_Owl_Service)

I only rated this four stars when I reviewed it.
At the time of this writing, I'm going back and rating it 5 stars because I still think about it a lot.
It is so dreamy and otherworldly.
The story of Blodeuwedd is so tragic.
It quickly became one of my favorite stories.
I think I first learned of her story from a [What's So Cool hack](https://itch.io/jam/what-is-so-cool-about-jam/entries), but I can't find it now.

Anyway this book is just great.
The ending is magical.

I think about the line, "She wants to be flowers but you keep making her owls" all the time.

## Best Horror Anthology

[The King in Yellow and Other Horror Stories, Robert W. Chambers](https://www.goodreads.com/book/show/129798.The_King_in_Yellow_and_Other_Horror_Stories)

I didn't actually finish this one.
But the four core King in Yellow stories were good.
*Repairer of Reputations* and *King in Yellow* particularly.
Chambers did some perfect Lovecraftian horror long before Lovecraft was even writing.
They stay with you and kind of make you itch.

## Best Anarchist Sci-Fi

[The Dispossessed: An Ambiguous Utopia Ursula K. Le Guin](https://www.goodreads.com/book/show/13651.The_Dispossessed)

In trying her hardest to truly imagine a communist anarchist upotia, Le Guin could do no better than banishing them to the moon.

Such a great story though. Fiercely feminist and anti-capitalist. But also realistic and critical of anarchism.

I ran into this with her *A Wizard Of Earthsea* too, but her writing is so measured and even and matter-of-fact that it can be kind of hypnotic to me. For example, there was one scene of extreme violence in this book, and it just kind of happened and then we dealt with it and moved on, and I was wholly unprepared for it.

## Best Fantasy

It's a tie!

[Imajica, Clive Barker](https://www.goodreads.com/book/show/567704.Imajica)

I can't tell you the whole plot any longer, but the characters and the world and the cities and the landscapes and the monsters are all burned into my brain.

[Piranesi, Susanna Clarke](https://www.goodreads.com/book/show/52702097-piranesi)

Short, dreamy, transporting. Just like *Imagjica!* The House is so hauntingly evocative.

## Best Computer Science Book

[Let Over Lambda, Doug Hoyte](https://www.goodreads.com/book/show/4004178-let-over-lambda)

I've hacked around with lisps off and on over the years enough to appreciate their expressiveness and their regular syntax, but struggled to appreciate lisp's more powerful features. Namely macros. When I mentioned this to my lispy friends, they unfailingly recommended this book to me. My only regret is not reading it much earlier.

## Colophon

Here's how I review my book data at the end of the year.

1. Go to goodreads. Click on 'My Books'. Scroll down to the bottom of the sidebar and click on 'Import and Export'. Export your library.

2. Oh yeah, install [csvkit](https://csvkit.readthedocs.io/en/latest/index.html)

3. Now you can csvkit your dataset.

      For example, show me books I read in 2022 that are shelved as "ebooks" and sort them by rating:

      ```
      csvcut -c 2,8,12,16,17 export.csv \
      | csvgrep -c 4 -r "^2022" \
      | csvgrep -c 5 -m 'ebook' \
      | csvsort -c 2
      ```

      `-c` values are column numbers. the `-r` flag searches for a regex. and `-m` just Matches a pattern.

changequote`'dnl change quotes `back to default'
include(src/footer.html)
