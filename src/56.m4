define(__timestamp, 2024-09-10)dnl
define(__title, `Web Applications and Web Documents')dnl
define(__subtitle, `a mental model for doing stuff on the internet')dnl
define(__keywords, `potpourri, smolnet')dnl
define(__id, 56)dnl
include(src/header.html)
changequote(<!,!>)dnl Or else `code blocks` confuse m4

A mental model that I have been using a lot lately
distinguishes between two types of content:
web applications
and web documents.

Web applications---like
other kinds of applications---require
an operating system to run them.
In this case that OS is the web browser.
Anything that I explicitly need to fire up firefox for---banking,
shopping, etc---is
a web application.
(Often this is synonymous with anything
that makes heavy use of javascript.)

Web documents on the other hand
are the purview of bloggers,
wikis, threads, etc.
They are served over gopher,
gemini, and http/s.
I consume the majority of this content
via an rss feed reader.
Or if I'm looking for a quick answer to a question,
I'll hit duckduckgo in w3m or lynx
and skim a stackoverflow page
or reddit thread.

This has provided me a vocabulary
similar to "small web" vs "large web,"
but which feels both more natural and more useful to me.
I prefer to read web documents
via text browsers and other CLI/TUI tools.
And when I need to do some kind of web application work,
that's when I tell myself I need to leave the land of text and documents,
and go boot up my webOS.

changequote`'dnl change quotes `back to default'
include(src/footer.html)
