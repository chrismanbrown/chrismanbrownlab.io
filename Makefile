LATEST := $(shell cat LATEST)
SRCS=$(shell find src -name '*.m4' ! -name "feed.m4")
OUTS=$(SRCS:src/%.m4=%.html)
vpath %.m4 src
vpath %.html www

all: $(OUTS) list.html www/rss.xml index.html

www/index.html: src/$(LATEST).m4
	sh bin/abbr.sh $< \
		| m4 -D__latest=$(LATEST) \
		| pandoc -f markdown+autolink_bare_uris -t html5 \
		> $@

www/rss.xml: src/feed.m4
	m4 -D__latest=$(LATEST) $< > $@

%.html: %.m4
	sh bin/abbr.sh $< \
		| m4 -D__latest=$(LATEST) \
		| pandoc -f markdown+autolink_bare_uris -t html5 \
		> www/$@
